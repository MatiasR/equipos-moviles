var mockData = {};

mockData.todosLosModelos = {};
mockData.todosLosModelos = [{
    "Codigo": "I70",
    "Marca": { "Nombre": "Samsung", "Id": "0" },
    "Nombre": "S4",
    "Color": "Negro",
    "Capacidad": "4GB",
    "Descripcion": "Nuevo"
}, {
    "Codigo": "I70",
    "Marca": { "Nombre": "Samsung", "Id": "0" },
    "Nombre": "S4",
    "Color": "Negro",
    "Capacidad": "4GB",
    "Descripcion": "Usado"
}];

mockData.todasLasMarcas = {};
mockData.todasLasMarcas = [{
    "Nombre": "Apple",
    "Id": 7
}, {
    "Nombre": "Samsung",
    "Id": 0
}, {
    "Nombre": "SonyEriccson",
    "Id": 70
},];