'use strict';

describe('Service: altaEquipoOpcionesModal', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var altaEquipoOpcionesModal;
  beforeEach(inject(function (_altaEquipoOpcionesModal_) {
    altaEquipoOpcionesModal = _altaEquipoOpcionesModal_;
  }));

  it('should do something', function () {
    expect(!!altaEquipoOpcionesModal).toBe(true);
  });

});
