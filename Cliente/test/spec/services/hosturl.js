'use strict';

describe('Service: hostUrl', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var hostUrl;
  beforeEach(inject(function (_hostUrl_) {
    hostUrl = _hostUrl_;
  }));

  it('should do something', function () {
    expect(!!hostUrl).toBe(true);
  });

});
