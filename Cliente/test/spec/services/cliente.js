'use strict';

describe('Service: cliente', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var cliente;
  beforeEach(inject(function (_clienteService_) {
    cliente = _clienteService_;
  }));

  it('should do something', function () {
    expect(!!cliente).toBe(true);
  });

});
