'use strict';

describe('Service: equipo', function () {

  // load the service's module
  beforeEach(module('clienteApp'));
   var hostUrl,
  httpBackend,
  equipoService;

  // instantiate service
  var equipo;
  beforeEach(inject(function (_equipoService_, _hostUrl_, $httpBackend) {
    equipoService = _equipoService_;
    hostUrl = _hostUrl_;
    httpBackend = $httpBackend;
  }));

  it('el servicio deberia tener las variables seteadas correctamente', function () {
    expect(!!equipoService).toBe(true);
    expect(!!httpBackend).toBe(true);
  });

   it('el servicio deberia devolver todos los equipos', function () {
    httpBackend.expectGET(hostUrl+'/api/equipos').respond(200, mockDataEquipos.todosLosEquipos);    
    equipoService.obtenerEquipos().then(
      function(p){
        expect(p.data.length).toEqual(mockDataEquipos.todosLosEquipos.length);
      },
      function(error){
        throw new Error('Test Fail');
      });
    httpBackend.flush();

  });

});
