'use strict';

describe('Service: confirmacionModales', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var confirmacionModales;
  beforeEach(inject(function (_confirmacionModales_) {
    confirmacionModales = _confirmacionModales_;
  }));

  it('should do something', function () {
    expect(!!confirmacionModales).toBe(true);
  });

});
