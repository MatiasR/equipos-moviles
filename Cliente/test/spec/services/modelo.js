'use strict';

describe('Service: modelo', function () {
 
  // load the service's module
  beforeEach(module('clienteApp'));
   var hostUrl,
  httpBackend,
  modeloService;

  beforeEach(inject(function (_hostUrl_, $httpBackend, _modeloService_) {
    modeloService = _modeloService_;
    hostUrl = _hostUrl_;
    httpBackend = $httpBackend;
  }));

  it('el servicio deberia tener las variables seteadas correctamente', function () {
    expect(!!modeloService).toBe(true);
    expect(!!httpBackend).toBe(true);
  });

  it('el servicio deberia devolver los modelos', function () {
    httpBackend.expectGET(hostUrl+'/api/modelos').respond(200, mockData.todosLosModelos);
    modeloService.obtenerModelos().then(
      function(p){
        expect(p.data.length).toEqual(mockData.todosLosModelos.length);
      },
      function(error){
        throw new Error('Test Fail');
      });
    httpBackend.flush();

  });

  it('el servicio deberia devolver las marcas', function () {
    httpBackend.expectGET(hostUrl+'/api/marcas').respond(200, mockData.todasLasMarcas);
    modeloService.obtenerMarcas().then(
      function(p){
        expect(p.data.length).toEqual(mockData.todasLasMarcas.length);
      },
      function(error){
        throw new Error('Test Fail');
      });
    httpBackend.flush();

  });

});
