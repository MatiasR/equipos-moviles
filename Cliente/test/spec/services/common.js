'use strict';

describe('Service: common', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var common;
  beforeEach(inject(function (_commonService_) {
    common = _commonService_;
  }));

  it('should do something', function () {
    expect(!!common).toBe(true);
  });

  it('el servicio deberia devolver una fecha', function () {
    var test = common.getNowDate();
    expect(test).toBeDefined(true);
  });

});