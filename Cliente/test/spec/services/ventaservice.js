'use strict';

describe('Service: ventaService', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var ventaService;
  beforeEach(inject(function (_ventaService_) {
    ventaService = _ventaService_;
  }));

  it('should do something', function () {
    expect(!!ventaService).toBe(true);
  });

});
