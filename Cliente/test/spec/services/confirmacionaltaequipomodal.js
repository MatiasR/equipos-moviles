'use strict';

describe('Service: confirmacionAltaEquipoModal', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var confirmacionAltaEquipoModal;
  beforeEach(inject(function (_confirmacionAltaEquipoModal_) {
    confirmacionAltaEquipoModal = _confirmacionAltaEquipoModal_;
  }));

  it('should do something', function () {
    expect(!!confirmacionAltaEquipoModal).toBe(true);
  });

});
