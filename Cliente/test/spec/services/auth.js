'use strict';

describe('Service: auth', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var auth, httpBackend, hostUrl, cookies;
  beforeEach(inject(function (_auth_, $httpBackend, _hostUrl_, $cookies) {
    auth = _auth_;
    httpBackend = $httpBackend;
    hostUrl = _hostUrl_;
    cookies = $cookies;
  }));

  it('deberia estar definido', function () {
    expect(!!auth).toBe(true);
  });

  it('deberia loguear un usuario', function(){
    var token = "a8473492-55df-47ff-bda7-832dba98529a";
    var user = { nickName: 'Matt', password: 'p' };
    httpBackend.expectPOST(hostUrl + '/api/login', user).respond(200, token);
    
    auth.login(user).
      success(function(data){
        expect(data).toBe(token);
      }).
      error(function(e){
        throw new Error('Test Fail');
      });
    httpBackend.flush();
  });

});
