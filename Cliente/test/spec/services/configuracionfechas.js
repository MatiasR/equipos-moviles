'use strict';

describe('Service: configuracionFechas', function () {

  // load the service's module
  beforeEach(module('clienteApp'));

  // instantiate service
  var configuracionFechas;
  beforeEach(inject(function (_configuracionFechas_) {
    configuracionFechas = _configuracionFechas_;
  }));

  it('should do something', function () {
    expect(!!configuracionFechas).toBe(true);
  });

});
