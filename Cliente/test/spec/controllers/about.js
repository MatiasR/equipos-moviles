'use strict';

describe('Controller: AboutCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var AboutCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AboutCtrl = $controller('AboutCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  // it('la fecha deberia estar seteada', function () {
  //   expect(scope).toBeDefined();
  //   expect(scope.year).toBeDefined();
  // });
});
