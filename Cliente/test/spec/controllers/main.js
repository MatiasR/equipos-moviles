'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var MainCtrl,
    scope, 
    commonService;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope,_commonService_) {
    scope = $rootScope.$new();
    commonService = _commonService_;
   
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('deberia setear la fecha del scope', function () {
    expect(scope.year).toBeDefined(true);
  });
});
