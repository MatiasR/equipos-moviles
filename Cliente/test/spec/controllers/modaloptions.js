'use strict';

describe('Controller: ModaloptionsCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var modalController,
  scope,
    modalInstance;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();

    modalController = $controller('ModaloptionsCtrl', {
      $scope: scope,
      $uibModalInstance: modalInstance     
      // place here mocked dependencies
    });
  }));

  it('deberia instanciar el controlador para los modales de confirmacion', function () {
    expect(!!modalController).toBe(true);
  });
});
