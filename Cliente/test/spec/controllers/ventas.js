'use strict';

describe('Controller: VentasCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var VentasCtrl,
    scope,
    httpBackend,
    hostUrl,
    modalResult,
    modal,
    notification;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $httpBackend, _hostUrl_) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    hostUrl = _hostUrl_;
    //Seteo de Modales y toastrs
    modalResult = {
      then: function (success, fail) {
        modalResultOk = success;
        modalResultCancel = fail;
      }
    };
    modal = {
      open: function (opc) {
        return { result: modalResult };
      }
    };
    notification = {
      success: function (message) { },
      error: function (message) { },
      warning: function (message) { }
    };
    //init spies
    spyOn(modal, 'open').and.callThrough();
    spyOn(modalResult, 'then').and.callThrough();
    spyOn(notification, 'success').and.callThrough();
    spyOn(notification, 'error').and.callThrough();
    //END - Seteo de Modales y toastrs

    VentasCtrl = $controller('VentasCtrl', {
      $scope: scope,
      notificationService: notification,
      $uibModal: modal
    });
  }));

  it('deberia cargar las ventas al inicializar', function () {
    //Implementar una vez creado el controller
    expect(mockDataVentas.todasLasVentas).toBeDefined();
  });
});
