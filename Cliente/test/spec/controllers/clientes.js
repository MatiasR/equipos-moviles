'use strict';

describe('Controller: ClientesCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var ClientesCtrl,
    httpBackend,
    hostUrl,
    modalResult,
    modal,
    notification,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $httpBackend, _hostUrl_) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    hostUrl = _hostUrl_;
    //Seteo de Modales y toastrs
    modalResult = {
      then: function (success, fail) {
        modalResultOk = success;
        modalResultCancel = fail;
      }
    };
    modal = {
      open: function (opc) {
        return { result: modalResult };
      }
    };
    notification = {
      success: function (message) { },
      error: function (message) { },
      warning: function (message) { }
    };
    //init spies
    spyOn(modal, 'open').and.callThrough();
    spyOn(modalResult, 'then').and.callThrough();
    spyOn(notification, 'success').and.callThrough();
    spyOn(notification, 'error').and.callThrough();
    //END - Seteo de Modales y toastrs

    ClientesCtrl = $controller('ClientesCtrl', {
      $scope: scope,
      notificationService: notification,
      $uibModal: modal
      // place here mocked dependencies
    });
  }));

  it('deberia cargar los clientes al inicializar', function () {
    httpBackend.expectGET(hostUrl + '/api/clientes').respond(200, mockDataClientes.todosLosClientes);
    httpBackend.flush();

    expect(scope.clientes).toBeDefined();
    expect(scope.clientes.length).toBe(mockDataClientes.todosLosClientes.length);
  });
});
