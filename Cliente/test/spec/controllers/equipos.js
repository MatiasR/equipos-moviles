'use strict';

describe('Controller: EquiposCtrl', function () {

  // load the controller's module
  beforeEach(module('clienteApp'));

  var EquiposCtrl,
    httpBackend,
    hostUrl,
    modalResult,
    modal,
    notification,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $httpBackend, _hostUrl_) {
    scope = $rootScope.$new();
    httpBackend = $httpBackend;
    hostUrl = _hostUrl_;
    //Seteo de Modales y toastrs
    modalResult = {
      then: function (success, fail) {
        modalResultOk = success;
        modalResultCancel = fail;
      }
    };
    modal = {
      open: function (opc) {
        return { result: modalResult };
      }
    };
    notification = {
      success: function (message) { },
      error: function (message) { },
      warning: function (message) { }
    };
    //init spies
    spyOn(modal, 'open').and.callThrough();
    spyOn(modalResult, 'then').and.callThrough();
    spyOn(notification, 'success').and.callThrough();
    spyOn(notification, 'error').and.callThrough();
    //END - Seteo de Modales y toastrs

    EquiposCtrl = $controller('EquiposCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('deberia cargar los equipos al inicializar', function () {
    httpBackend.expectGET(hostUrl + '/api/equipos').respond(200, mockDataEquipos.todosLosEquipos);
    httpBackend.flush();

    expect(scope.equipos).toBeDefined();
    expect(scope.equipos.length).toBe(mockDataEquipos.todosLosEquipos.length);
  });
});
