'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcionesGrillaMisEquipos
 * @description
 * # opcionesGrillaMisEquipos
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcionesGrillaMisEquipos', {
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    noUnselect : true,
    excessRows: 10,
    enableRowSelection: true,
    multiSelect : false,
    columnDefs: [
    //{ field: 'equipoId', visible: false },
    { field: 'Id', visible: false },
    { field: 'Modelo.Id', visible: false },
    { field: 'Modelo.Marca.Id', visible: false },
    { field: 'Imei', displayName: 'IMEI' }, 
    { field: 'Modelo.Marca.Nombre',  displayName: 'Marca'},
    { field: 'Modelo.Nombre', displayName: 'Modelo' },
    { field: 'Modelo.Color', displayName: 'Color' },
    { field: 'Modelo.Capacidad', displayName: 'Caracteristicas'},
    { field: 'Modelo.Descripcion', displayName: 'Descripcion'},  
    { field: 'Precio',  displayName: 'Precio'},
    { field: 'Descripcion',  displayName: 'Comentarios'},
       
    ],
    data: 'equipos'
});