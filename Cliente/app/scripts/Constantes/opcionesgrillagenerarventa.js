'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcionesGrillaGenerarVenta
 * @description
 * # opcionesGrillaGenerarVenta
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcionesGrillaGenerarVenta',{
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    excessRows: 10,
    columnDefs: [    
    { field: 'telefono', displayName: 'Telefono'},
    { field: 'marca', displayName: 'Marca', visible:false}, 
    { field: 'modelo', displayName: 'Modelo', visible:false },  
    { field: 'precio', displayName: 'Precio' }, 
    { field: 'cantidad',  displayName: 'Cantidad'}, 
    { field: 'cuotas',  displayName: 'Cuotas'},
    { field: 'valorCuota',  displayName: 'Valor Cuota'},
    { field: 'descripcionVenta',  displayName: 'Descripcion', visible:false}    
    ],
    data: []
});