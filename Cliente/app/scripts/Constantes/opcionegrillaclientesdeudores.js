'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcioneGrillaClientesDeudores
 * @description
 * # opcioneGrillaClientesDeudores
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcioneGrillaClientesDeudores', {
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    excessRows: 10,
    columnDefs: [
    { field: 'clienteId', visible: false },
    { field: 'Apellido', displayName: 'Apellido'},
    { field: 'nombre', displayName: 'Nombre'}, 
    { field: 'telefono', displayName: 'Telefono' },  
    { field: 'dni', displayName: 'DNI' }, 
    { field: 'email',  displayName: 'Email'}, 
    { field: 'direccion',  displayName: 'Direccion'},
     { field: 'detalle', 
          displayName: 'Detalle',
          //ver como diagramar el modal 
          cellTemplate:'<div class="ui-grid-cell-contents"><a class="btn btn-link" ng-click="grid.appScope.showErrors(row.entity)" ng-show="!!row.entity.errors">Show more</a></div>',
          cellTooltip: true, 
          width:350 
        }   
    ],
    data: []
});