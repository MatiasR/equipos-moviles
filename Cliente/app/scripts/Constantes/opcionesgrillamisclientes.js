'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcioneGrillaMisMejoresClientes
 * @description
 * # opcioneGrillaMisMejoresClientes
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcionesGrillaMisClientes',{
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    excessRows: 10,
    noUnselect : true,
    enableRowSelection: true,
    multiSelect : false,
    columnDefs: [
    { field: 'Id', visible: false },
    { field: 'Apellido', displayName: 'Apellido'},
    { field: 'Nombre', displayName: 'Nombre'}, 
    { field: 'Telefono', displayName: 'Telefono' },  
    { field: 'Dni', displayName: 'DNI' }, 
    { field: 'Email',  displayName: 'Email'}, 
    { field: 'Direccion',  displayName: 'Direccion'},
    { field: 'Cantidad',  displayName: 'Compras', visible: false }    
    ],
    data: 'clientes'
});