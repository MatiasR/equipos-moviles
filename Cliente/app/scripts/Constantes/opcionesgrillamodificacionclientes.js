'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcionesGrillaModificacionClientes
 * @description
 * # opcionesGrillaModificacionClientes
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcionesGrillaModificacionClientes',{
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    excessRows: 10,
    columnDefs: [
    { field: 'clienteId', visible: false },
    { field: 'Apellido', displayName: 'Apellido'},
    { field: 'nombre', displayName: 'Nombre'}, 
    { field: 'telefono', displayName: 'Telefono' },  
    { field: 'dni', displayName: 'DNI' }, 
    { field: 'email',  displayName: 'Email'}, 
    { field: 'direccion',  displayName: 'Direccion'},
    { field: 'cantidad',  displayName: 'Equipos'}    
    ],
    data: []
});