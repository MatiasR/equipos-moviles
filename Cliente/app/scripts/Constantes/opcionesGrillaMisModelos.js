'use strict';

/**
 * @ngdoc service
 * @name clienteApp.opcionesGrillaMisModelos
 * @description
 * # opcionesGrillaMisModelos
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('opcionesGrillaMisModelos', {
    enableFiltering: true,
    enableColumnResize: true,
    exporterMenuPdf :false,        
    enableGridMenu: true,
    excessRows: 10,
    enableRowSelection: true,
    noUnselect : true,
    multiSelect : false,
    columnDefs: [
    // { field: 'equipoId', visible: false },
    // { field: 'modeloId', visible: false },
    // { field: 'codigo', visible: false },
    { field: 'Codigo', displayName: 'Codigo'},
    { field: 'Marca.Nombre',  displayName: 'Marca'}, 
    { field: 'Nombre', displayName: 'Nombre' },  
    { field: 'Color', displayName: 'Color'}, 
    { field: 'Capacidad',  displayName: 'Capacidad'},
    { field: 'Descripcion',  displayName: 'Descripcion'}
    ],
    data: 'modelos'
    //rowTemplate : '<div class="btn-group"><button type="button" class="btn btn-default">Action <span class="caret"></span></button>'
});