'use strict';

/**
 * @ngdoc directive
 * @name clienteApp.directive:eventoEnter
 * @description
 * # eventoEnter
 */
angular.module('clienteApp')
    .directive('eventoEnter', [function () {
        return {
            link: function (scope, $element, $attrs) {
                $element.bind("keydown keypress", function (event) {
                    if (event.which === 13 || event.which === 9 ) {
                        scope.$parent.buscarModelo();
                        event.preventDefault();
                    }
                });

            }
        }
    }]);