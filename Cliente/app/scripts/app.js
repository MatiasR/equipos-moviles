'use strict';

/**
 * @ngdoc overview
 * @name clienteApp
 * @description
 * # clienteApp
 *
 * Main module of the application.
 */
angular
  .module('clienteApp', [
    'ngRoute',
    'ngAnimate',
    'ngCookies',
    'ngResource',
  'angularMoment',
  'ui.bootstrap', 
  'ui.grid',
  'ui.grid.exporter',
  'ui.grid.resizeColumns',
  'ui.grid.autoResize',
  'ui.grid.selection',
  'toastr',  
  'app.directives',
  'app.localization',
  'http-auth-interceptor'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {redirectTo: '/pages/signin'}
      )
      .when('/main', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/moviles/misEquipos', {
        templateUrl: 'views/moviles/misequipos.html',
        controller: 'EquiposCtrl'
      })
      .when('/moviles/altaModelo', {
        templateUrl: 'views/modelos/mismodelos.html',
        controller: 'ModelosCtrl'
      })
      .when('/ventas/misVentas', {
        templateUrl: 'views/ventas/misventas.html',
        controller: 'VentasCtrl'
      })
      .when('/ventas/generarVentas', {
        templateUrl: 'views/ventas/generarventas.html',
        controller: 'VentasCtrl'
      })
      .when('/ventas/modificacionVenta', {
        templateUrl: 'views/ventas/modificacionventas.html',
        controller: 'VentasCtrl'
      })
      .when('/clientes/misClientes', {
        templateUrl: 'views/clientes/misclientes.html',
        controller: 'ClientesCtrl'
      })
      .when('/clientes/altaClientes', {
        templateUrl: 'views/clientes/altaclientes.html',
        controller: 'ClientesCtrl'
      })
      .when('/clientes/modificacionClientes', {
        templateUrl: 'views/clientes/modificacionclientes.html',
        controller: 'ClientesCtrl'
      })
      .when('/clientes/deudores', {
        templateUrl: 'views/clientes/clientesdeudores.html',
        controller: 'ClientesCtrl'
      })
      .when('/about', {
        templateUrl: 'views/singlePageStructure/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/404', {
        templateUrl: 'views/errorPages/404.html',
        controller: 'AboutCtrl'
      })
      .when('/pages/signin', {
        templateUrl: 'views/pages/signin.html'
      })
      .otherwise({
        redirectTo: '/404'
      });
  });
