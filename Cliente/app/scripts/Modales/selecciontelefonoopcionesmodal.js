'use strict';

/**
 * @ngdoc service
 * @name clienteApp.seleccionTelefonoOpcionesModal
 * @description
 * # seleccionTelefonoOpcionesModal
 * Value in the clienteApp.
 */
angular.module('clienteApp')
  .value('seleccionTelefonoOpcionesModal', { 
        animation: true,
        templateUrl: 'views/Modales/modalselecciontelefono.html',
        controller: 'ModaloptionsCtrl'
        //,
        //resolve: { 
          //strategy : null
        //}
      });

