'use strict';

/**
 * @ngdoc service
 * @name clienteApp.confirmacionAltaEquipoModal
 * @description
 * # confirmacionAltaEquipoModal
 * Value in the clienteApp.
 */
angular.module('clienteApp')
  .value('confirmacionAltaEquipoModal', {
        animation: true,
        templateUrl: 'views/Modales/modalConfirmacionAltaEquipo.html',
        controller: 'ModaloptionsCtrl'
        //,
        //resolve: { 
        //strategy : null
        //}
    });