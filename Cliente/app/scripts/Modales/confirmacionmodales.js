'use strict';

/**
 * @ngdoc service
 * @name clienteApp.confirmacionModales
 * @description
 * # confirmacionModales
 * Value in the clienteApp.
 */
angular.module('clienteApp')
    .value('confirmacionModales', {
        animation: true,
        templateUrl: 'views/Modales/modalConfirmacion.html',
        controller: 'ModaloptionsCtrl'
        //,
        //resolve: { 
        //strategy : null
        //}
    });