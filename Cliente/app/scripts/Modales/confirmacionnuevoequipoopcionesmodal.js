'use strict';

/**
 * @ngdoc service
 * @name clienteApp.seleccionNuevaMarcaOpcionesModal
 * @description
 * # seleccionNuevaMarcaOpcionesModal
 * Value in the clienteApp.
 */
angular.module('clienteApp')
  .value('confirmacionNuevoEquipoOpcionesModal', { 
        animation: true,
        templateUrl: 'views/Modales/modalconfirmacionaltaequipo.html',
        controller: 'ModaloptionsCtrl'
        //,
        //resolve: { 
          //strategy : null
        //}
      });