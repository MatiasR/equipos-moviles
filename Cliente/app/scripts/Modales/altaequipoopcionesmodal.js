'use strict';

/**
 * @ngdoc service
 * @name clienteApp.altaEquipoOpcionesModal
 * @description
 * # altaEquipoOpcionesModal
 * Value in the clienteApp.
 */
angular.module('clienteApp')
  .value('altaEquipoOpcionesModal', {
        animation: true,
        size: 'lg',
        templateUrl: 'views/moviles/altaequipos.html',
        controller: 'ModaloptionsCtrl'
        //,
        //resolve: { 
        //strategy : null
        //}
    });