'use strict';

/**
 * @ngdoc service
 * @name clienteApp.common
 * @description
 * # common
 * Factory in the clienteApp.
 */
angular.module('clienteApp')
  .factory('commonService', function ($window) {
    // Service logic
    // ...
    // Public API here
    return {
      getNowDate: function () {
        return moment().format("DD/MM/YYYY");
      },
      formatDate: function (fecha) {
        return moment(fecha).format("DD/MM/YYYY");
      },
      getGridInnerHeigth: function (heigth) {
        return heigth - 120;             
      }
    };
});