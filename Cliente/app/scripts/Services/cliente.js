'use strict';

/**
 * @ngdoc service
 * @name clienteApp.cliente
 * @description
 * # cliente
 * Service in the clienteApp.
 */
angular.module('clienteApp')
  .factory('clienteService', function (hostUrl, $http, $filter) {
    // Public API here
    return {
      obtenerClientes: function () {
        //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
        return $http.get(hostUrl + '/api/clientes');
      },
      obtenerCliente: function (dni) {
        //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
        return $http.get(hostUrl + '/api/clientes/' + dni);
      },
      agregarCliente: function (cliente) {
        //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
        return $http.post(hostUrl + '/api/cliente/', cliente);
      },
      modificarCliente: function (cliente) {
        return $http.put(hostUrl + '/api/cliente/', cliente);
      }
    };
  });
