'use strict';

/**
 * @ngdoc service
 * @name clienteApp.modelo
 * @description
 * # modelo
 * Service in the clienteApp.
 */
angular.module('clienteApp')
  .service('modeloService', function (hostUrl, $http) {
    // AngularJS will instantiate a singleton by calling "new" on this function
   return {
      buscarModelo: function (codigo) {
        return $http.get(hostUrl + '/api/modelo/' + codigo);
      },
      obtenerModelos: function () {
        return $http.get(hostUrl + '/api/modelos');
      },
      agregarModelo: function (modelo) {
        return $http.post(hostUrl + '/api/modelo/', modelo);
      },
      obtenerMarcas: function () {
        return $http.get(hostUrl + '/api/marcas');
      },
      buscarModelosPorMarcas: function (marca) {
        return $http.get(hostUrl + '/api/marcas/' + marca);
      },
      modificarModelo: function (modelo) {
        return $http.put(hostUrl + '/api/modelo/', modelo);
      } 
    };
  });
