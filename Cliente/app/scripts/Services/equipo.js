'use strict';

/**
 * @ngdoc service
 * @name clienteApp.equipo
 * @description
 * # equipo
 * Service in the clienteApp.
 */
angular.module('clienteApp')
  .factory('equipoService', function (hostUrl, $http, $filter) {
    // Public API here
    return {
      obtenerEquipos: function () {
        //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
        return $http.get(hostUrl + '/api/equipos');
      },
      agregarEquipo: function (equipo) {
        return $http.post(hostUrl + '/api/equipos/', equipo);
      },
      modificarEquipo: function (equipo) {
        return $http.put(hostUrl + '/api/equipo/', equipo);
      },
      eliminarEquipo: function (imei) {
        return $http.delete(hostUrl + '/api/equipo/' + imei);
      }            
    };
  });
