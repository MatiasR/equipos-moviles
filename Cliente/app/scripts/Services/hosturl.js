'use strict';

/**
 * @ngdoc service
 * @name clienteApp.hostUrl
 * @description
 * # hostUrl
 * Constant in the clienteApp.
 */
angular.module('clienteApp')
  .constant('hostUrl', 'http://localhost:6262');
