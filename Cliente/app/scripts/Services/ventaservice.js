'use strict';

/**
 * @ngdoc service
 * @name clienteApp.ventaService
 * @description
 * # ventaService
 * Service in the clienteApp.
 */
angular.module('clienteApp')
  .factory('ventaService', function (hostUrl, $http, $filter) {
    // Public API here
    return {
      // obtenerClientes: function () {
      //   //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
      //   return $http.get(hostUrl + '/api/clientes');
      // },
      // obtenerCliente: function (dni) {
      //   //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
      //   return $http.get(hostUrl + '/api/clientes/' + dni);
      // },
      agregarVenta: function (venta) {
        //var dateToUrl = $filter('date')(strategy.lastRun, 'yyyyMMdd');
        return $http.post(hostUrl + '/api/venta/', venta);
      }
      // modificarCliente: function (cliente) {
      //   return $http.put(hostUrl + '/api/cliente/', cliente);
      // }
    };
  });
