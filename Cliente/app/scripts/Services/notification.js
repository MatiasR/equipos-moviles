'use strict';

/**
 * @ngdoc service
 * @name clienteApp.notification
 * @description
 * # notification
 * Service in the clienteApp.
 */
angular.module('clienteApp')
    .service('notification', function (toastr) {
        // Public API here
        return {
            success: function (message) {
                if (message == "") {
                    toastr.success(message, 'Ok', { tapToDismiss: true, timeOut: 6000 });
                } else {
                    toastr.success(message, { tapToDismiss: true, timeOut: 6000 });
                }
            },
            error: function (message) {
                var error = message || "Server caido. Contactar a IT";
                if (angular.isObject(message)) {
                    error = message.exceptionMessage || "Server caido. Contactar a IT";
                }
                console.log(error);
                toastr.error(error, 'Error', { tapToDismiss: true, extendedTimeOut: 0, timeOut: 0, closeButton: true });
            },
            warning: function (message) {
                toastr.warning(message, 'Warning', { tapToDismiss: true, extendedTimeOut: 0, timeOut: 0, closeButton: true });
            }
        };
    });