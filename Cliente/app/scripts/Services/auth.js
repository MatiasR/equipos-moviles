'use strict';

/**
 * @ngdoc service
 * @name clienteApp.auth
 * @description
 * # auth
 * Factory in the clienteApp.
 */
angular.module('clienteApp')
  .factory('auth', function ($http, hostUrl, $cookies, authService) {
    var tokenKey = 'moviles-token';
    var usrKeyExpirationDate = 'moviles-usr-Expiration';
    var usrKeyNombre = 'moviles-usr-Nombre';
    var usrKeyRol = 'moviles-usr-Rol';
    var usrKeyUserName = 'moviles-usr-UserName';
    
    return {
      login: function(user) {
        return $http.post(hostUrl + '/api/login', user);
        
      },
      logout: function(token) {
        return $http.post(hostUrl + '/api/auth/logout/' + token);
      },
      getUsrToken: function() {
        return $cookies.get(usrKeyUserName);
      },
      getToken: function() {
        return $cookies.get(tokenKey);
      },
      setToken: function(token) {
        this.token = token;
        //save token as a cookie
        $http.defaults.headers.common['Authorization'] = token;
        $cookies.put(tokenKey, token);
			  authService.loginConfirmed(token);
      },
      setUsr: function(usuario) {
        //save token as a cookie
        $cookies.put(usrKeyExpirationDate, usuario.ExpirationDate);
        $cookies.put(usrKeyNombre, usuario.Nombre);
        $cookies.put(usrKeyRol, usuario.Rol);
        $cookies.put(usrKeyUserName, usuario.UserName);
        //this.user = usuario;
      },
      //Gets de usuario
      getUsrExpirationDate: function() {
        //save token as a cookie
        return $cookies.get(usrKeyExpirationDate);
      },
      getUsrNombre: function() {
        //save token as a cookie
        return $cookies.get(usrKeyNombre);
      },
      getUsrRol: function() {
        //save token as a cookie
        return $cookies.get(usrKeyRol);
      },
      getUsrUserName: function() {
        //save token as a cookie
        return $cookies.get(usrKeyUserName);
      },
      //END - Gets usuario
      cleanToken: function(token) {
        this.token = null;
			  this.user = null;
			  $http.defaults.headers.common['Authorization'] = null;
			  $cookies.remove(tokenKey);
        // $cookies.remove(usrKeyExpirationDate);
        // $cookies.remove(usrKeyNombre);
        // $cookies.remove(usrKeyRol);
        // $cookies.remove(usrKeyUserName);
			  authService.loginCancelled();
      },
      isAuthenticated : function() {
        return !!this.getToken();
      }
    };
  });
