'use strict';

/**
 * @ngdoc service
 * @name clienteApp.configuracionFechas
 * @description
 * # configuracionFechas
 * Value in the clienteApp.
 */
angular.module('clienteApp')
  .value('configuracionFechas', {
    //cellFilter : 'date:"medium" : "EST"',
    //dateFormat: 'medium',
    //timeZone: 'EST',
   // cellFilter: 'amTimezone: "America/New_York" | amDateFormat:"MMM D, YYYY h:mm:ss a"',
    cellFilter: 'estDate',
    format: 'MMM D, YYYY h:mm:ss a',
    timeZone: 'America/New_York'
  });
