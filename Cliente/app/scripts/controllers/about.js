'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
  .controller('AboutCtrl', function ($scope, commonService) {
    $scope.year = commonService.getNowDate().getUTCFullYear();
});