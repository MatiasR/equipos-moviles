'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:VentasCtrl
 * @description
 * # VentasCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
  .controller('VentasCtrl', function ($scope, $uibModal, ventaService, confirmacionModales,clienteService, opcionesGrillaMisClientes, equipoService, notification, seleccionTelefonoOpcionesModal, opcionesGrillaMisEquipos) {

    $scope.opcionesGrillaMisEquipos = opcionesGrillaMisEquipos;
    $scope.opcionesGrillaMisClientes = opcionesGrillaMisClientes;
    $scope.venta = {
      Id: "",
      Fecha: "",
      Precio: "",
      Cliente: {
        Id: 0,
        Dni: "",
        Apellido: "",
        Nombre: "",
        Telefono: "",
        Email: "",
        Direccion: ""
      },
      Equipo: {
        Imei: "",
        Precio: "",
        Descripcion: "",
        Modelo: {
          Codigo: ""
        }
      },
    };

    $scope.descripcionCliente="Seleccione un Cliente";
    $scope.descripcionEquipo="Seleccione un Equipo";
    
    $scope.aceptarSeleccionTelefono = function (equipo) {
      $scope.venta.equipo = equipo;
    }

    $scope.cargarEquipos = function () {
      equipoService.obtenerEquipos().
        success(function (equipos) {
          $scope.equipos = equipos;
        }).
        error(function (e) {
          notification.error("Error");
        });
    }

    $scope.cargarClientes = function () {
      clienteService.obtenerClientes().
        success(function (clientes) {
          $scope.clientes = clientes;
        }).
        error(function (e) {
          notification.error("Error al iniciar");
        });
    }

    $scope.seleccionarTelefono = function () {
      $uibModal.open({
        animation: true,
        size: 'lg',
        templateUrl: 'views/Modales/modalselecciontelefono.html',
        controller: 'ModaloptionsCtrl',
        scope: $scope
      }).result.then(function () {

        $uibModal.open(confirmacionModales).result
          .then(function () {
            console.log($scope.venta);
            $scope.descripcionEquipo= $scope.venta.Equipo.Modelo.Marca.Nombre + " "+ $scope.venta.Equipo.Modelo.Color + " "+ $scope.venta.Equipo.Modelo.Capacidad  + " " + $scope.venta.Equipo.Imei; 
            // $scope.aceptarSeleccionTelefono();
          }, function () {
            $scope.seleccionarTelefono();
            $scope.venta.Equipo = $scope.setearEquipoEnNulo();
          });
      }, function () {
        //cancelar 
      });
      $scope.cargarEquipos();
    }

    $scope.seleccionarCliente = function () {
      $uibModal.open({
        animation: true,
        size: 'lg',
        templateUrl: 'views/Modales/modalseleccioncliente.html',
        controller: 'ModaloptionsCtrl',
        scope: $scope
      }).result.then(function () {

        $uibModal.open(confirmacionModales).result
          .then(function () {
            console.log($scope.venta);
            // $scope.aceptarSeleccionTelefono();
         $scope.descripcionCliente = $scope.venta.Cliente.Nombre + " "+ $scope.venta.Cliente.Apellido + " "+ $scope.venta.Cliente.Dni;
            
          }, function () {
            $scope.seleccionarCliente();
            $scope.venta.Cliente = $scope.setearClienteEnNulo();
          });
      }, function () {
        //cancelar 
      });
      $scope.cargarClientes();
    }

  $scope.agregarVenta = function () {
            $scope.venta.Fecha= new Date(Date.now()).toLocaleString();
            ventaService.agregarVenta($scope.venta).
                success(function () {
                    notification.success("Funco");

                }).
                error(function (e) {
                    notification.error("Error al iniciar");
                });
        }

    $scope.setearEquipoEnNulo = function () {
      $scope.venta.Equipo.Imei = "";
      $scope.venta.Equipo.Precio = "";
      $scope.venta.Equipo.Precio = "";
      $scope.venta.Equipo.Precio = "";
      $scope.venta.Equipo.Descripcion = "";
      $scope.venta.Equipo.Modelo.Codigo = "";
    };

    $scope.setearClienteEnNulo = function () {
      $scope.venta.Cliente.Id = "";
      $scope.venta.Cliente.Dni = "";
      $scope.venta.Cliente.Apellido = "";
      $scope.venta.Cliente.Nombre = "";
      $scope.venta.Cliente.Telefono = "";
      $scope.venta.Cliente.Email = "";
      $scope.venta.Cliente.Direccion = "";
    };

    $scope.opcionesGrillaMisEquipos.onRegisterApi = function (opcionesGrillaMisEquipos) {
      opcionesGrillaMisEquipos.selection.on.rowSelectionChanged($scope, function (row) {
        $scope.venta.Equipo = row.entity;
      });
    };

     $scope.opcionesGrillaMisClientes.onRegisterApi = function (opcionesGrillaMisClientes) {
      opcionesGrillaMisClientes.selection.on.rowSelectionChanged($scope, function (row) {
        $scope.venta.Cliente = row.entity;
      });
    };

    $scope.setearVenta = function () {
      $scope.venta = {
        Id: "",
        Fecha: "",
        Precio: "",
        Cliente: {
          Id: 0,
          Dni: "",
          Apellido: "",
          Nombre: "",
          Telefono: "",
          Email: "",
          Direccion: ""
        },
        Equipo: {
          Imei: "",
          Precio: "",
          Descripcion: "",
          Modelo: {
            Codigo: ""
          }
        },
      };
    };
  });
