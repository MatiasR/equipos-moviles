'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:ModelosCtrl
 * @description
 * # ModelosCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
  .controller('ModelosCtrl', function ($scope, $window, $uibModal, commonService, notification, modeloService, opcionesGrillaMisModelos, confirmacionModales) {
    $scope.innerHeigth = commonService.getGridInnerHeigth($window.innerHeight);
        $scope.opcionesGrillaMisModelos = opcionesGrillaMisModelos;
        
        $scope.modelos = null;
        $scope.modelo = {
            Codigo:"",
            Marca:{Nombre:"", Id:""},
            Nombre:"",
            Color:"",
            Capacidad:"",
            Descripcion:""
        };

        $scope.habilitarEdicion = true;
        $scope.marcas ={};
        
        $scope.cargarModelos = function () {
            modeloService.obtenerModelos().
                success(function (modelos) {
                    $scope.modelos = modelos;
                }).
                error(function (e) {
                    notification.error("Error al iniciar"); 
                });
        }
        //Setea el enable de los botones de edicion
        $scope.opcionesGrillaMisModelos.onRegisterApi = function(opcionesGrillaMisModelos){
             opcionesGrillaMisModelos.selection.on.rowSelectionChanged($scope,function(row){
                $scope.habilitarEdicion = false;
                $scope.modelo =  row.entity;
            });
        };
        
        $scope.buscarModelo = function () {
            //llamo a servicio que devuelva un modelo para un codigo y lo asigno a $scope.equipo.modelo
            modeloService.buscarModelo($scope.modelo.Codigo).
                success(function (modelo) {
                    if(modelo != null){
                        var codigoErroneo = $scope.modelo.Codigo;
                        $scope.modelo.Codigo = "";
                        notification.warning("El codigo '" + codigoErroneo + "' ya se encuentra cargado"); 
                    }
                }).
                error(function (e) {
                    notification.error("Error"); 
                });
        }
        
        $scope.abrirModalAltaModelo = function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/modelos/altamodelo.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                              console.log($scope.marcaSeleccionada);                                                      
                                                $uibModal.open(confirmacionModales).result
                                                        .then(function () {
                                                            if($scope.modelo.Marca.Id == undefined){
                                                               $scope.modelo.Marca = {
                                                                        "Nombre":$scope.modelo.Marca,
                                                                        "Id":0};
                                                            }
                                                            $scope.altaModelo($scope.modelo);
                                                        }, function () {
                                                            $scope.abrirModalAltaModelo();
                                                        });                                              
                                                    }, function () {
                                                        //cancelar 
                                                    });
                                                    $scope.cargarMarcas();
        }
        
        $scope.cargarMarcas = function () {
            modeloService.obtenerMarcas().
                success(function (marcas) {
                    $scope.marcas = marcas;
                }).
                error(function (e) {
                    notification.error("Error al cargar marcas"); 
                });
        }
        
        $scope.altaModelo = function (modelo) {
            modeloService.agregarModelo(modelo).
                success(function () {
                    notification.success("Se agrego el modelo con exito");
                    $scope.cargarModelos();
                    $scope.setearModelo();
                }).
                error(function (e) {
                    notification.error("Error al agregar el modelo"); 
                });
        }
        
        $scope.buscarModelosPorMarcas = function () {
            //no se por que tira un error cuando le aplico la funcion en el campo modelo del html con el evento enter
        }
        
        $scope.abrirModalModificarModelo =  function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/modelos/modificacionmodelo.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                                
                                                $uibModal.open(confirmacionModales).result
                                                        .then(function () {
                                                            $scope.modificarModelo($scope.modelo);
                                                        }, function () {
                                                            $scope.abrirModalModificarModelo();
                                                        });                                              
                                                    }, function () {
                                                        //cancelar 
                                                    });
        }
        
        $scope.modificarModelo = function () {
            modeloService.modificarModelo($scope.modelo).
                success(function () {
                    notification.success("Se modifico el modelo con exito");
                    $scope.setearModelo(); 
                }).
                error(function (e) {
                    notification.error("Error al modificar el modelo"); 
                });
        }
        
        $scope.setearModelo = function () {
            $scope.modelo = {
                Codigo: "",
                Marca: { Nombre: "", Id: "" },
                Nombre: "",
                Color: "",
                Capacidad: "",
                Descripcion: ""
            };
        };
        
        //Inicio
        $scope.cargarModelos();
  });