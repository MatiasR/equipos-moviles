'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:ModaloptionsCtrl
 * @description
 * # ModaloptionsCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
    .controller('ModaloptionsCtrl', function ($scope, $uibModalInstance, notification, $uibModal) {
               
        $scope.ok = function () {
            $uibModalInstance.close(false);
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });