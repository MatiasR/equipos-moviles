'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
  .controller('MainCtrl', function ($scope, $location, commonService, auth, $log, notification) {
    $scope.main = {
      brand: 'Moviles',
      version: '1.0.0.0',
      isConnected: false,
      user: { UserName: '', Password: '', Rol: '' }
    };

    $scope.year = commonService.getNowDate();

    $scope.isSpecificPage = function () {
      var path;
      path = $location.path();
      return _.contains(['/404', '/pages/500', '/pages/signin', '/pages/forgot', '/pages/lock-screen'], path);
    };

    $scope.login = function () {
      var cookieToken = auth.getToken();
      //Tengo token de logueo
      if (cookieToken != undefined && cookieToken != null) {
        var usuario = auth.getUsrUserName();
        //Si Tengo token usuario cargado
        if (usuario != undefined && usuario != null) {
          if (usuario == $scope.main.user.UserName) {
            var fechaActual = commonService.getNowDate();
            var expDate = auth.getUsrExpirationDate();
            if (expDate < fechaActual) {
              auth.cleanToken();
              $scope.verify();
            } else {
              //Redirecciono a main. Estuvo logueado, es el mismo user y el expiration no expiro
              $location.path('/main');
            }

          }
        }
        //Tenia token y no era el user ó no habia token  
        //auth.cleanToken();
        $scope.verify();

      } else {
        //No Hay token de logueo por ende se debe verificar.
        //auth.cleanToken();
        $scope.verify();
      }
    }

    $scope.verify = function () {
      var callback = $location.search().url;
      if (callback === undefined || callback === "/pages/signin") { callback = "" }
      auth.login($scope.main.user).
        success(function (usuario) {
          var fechaActual = commonService.getNowDate();
          if (usuario.Token != undefined && usuario.Token != null && (commonService.formatDate(usuario.ExpirationDate) > fechaActual)) {
            callback = '/main';
            auth.setToken(usuario.Token);
            auth.setUsr(usuario);
            $scope.main.user = usuario;
            $location.path(callback);
          } 
          // else {
          //   $scope.resetUser();
          // }

        }).
        error(function (e) {
          notification.error(e.ExceptionMessage);
          $log.info(e.ExceptionMessage);
        });
    };

    $scope.resetUser = function () {
      $scope.main.user.Password = "";
      $scope.main.user.Token = "";
      $scope.main.user.ExpirationDate = "";
      auth.cleanToken();
    }

    $scope.isAdmin = function () {
      //return auth.getUsrToken().Rol == 1;
      var usuarioToken = auth.getToken();
      if (usuarioToken != undefined && usuarioToken != null) {
        var rol = auth.getUsrRol();
        return (rol < "1");
      }
    }

    $scope.checkToken = function () {
      var tok = auth.getToken();
      var expDate = auth.getUsrExpirationDate();
      var callback = '/pages/signin';
      var fechaActual = commonService.getNowDate();

      if (tok == undefined || tok == undefined) {
        $location.path(callback);
      }

      if (expDate == undefined || expDate == undefined) {
        $location.path(callback);
      } else {
        if (expDate < fechaActual) {
          auth.cleanToken();
          $scope.verify();
        } else {
          //Redirecciono a main. Estuvo logueado y el expiration no expiro
          $location.path('/main');
        }
      }
    }

    $scope.checkToken();

  });