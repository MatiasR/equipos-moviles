'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:ClientesCtrl
 * @description
 * # ClientesCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
    .controller('ClientesCtrl', function ($scope, commonService, $uibModal, clienteService, notification, $window, opcionesGrillaMisClientes, confirmacionModales) {
        $scope.innerHeigth = commonService.getGridInnerHeigth($window.innerHeight);
    
        $scope.opcionesGrillaMisClientes = opcionesGrillaMisClientes;
        $scope.habilitarEdicion = true;
        $scope.cliente = {
            Id:0,
            Dni:"",
            Apellido:"",
            Nombre:"",
            Telefono:"",
            Email:"",
            Direccion:""
        };
        //Setea el enable de los botones de edicion
        $scope.opcionesGrillaMisClientes.onRegisterApi = function (opcionesGrillaMisClientes) {
            opcionesGrillaMisClientes.selection.on.rowSelectionChanged($scope, function (row) {
                $scope.habilitarEdicion = false;
                $scope.cliente = row.entity;
            });
        };

        $scope.cargarClientes = function () {
            clienteService.obtenerClientes().
                success(function (clientes) {
                    $scope.clientes = clientes;
                }).
                error(function (e) {
                    notification.error("Error al iniciar");
                });
        }
        
        $scope.abrirModalAltaCliente = function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/clientes/altaclientes.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                                $uibModal.open(confirmacionModales).result
                                                        .then(function () {
                                                            $scope.agregarCliente();
                                                        }, function () {
                                                            $scope.abrirModalAltaCliente();
                                                        });                                              
                                                    }, function () {
                                                        //cancelar 
                                                    });
        }
        
        $scope.buscarDni = function (e) {
            if (e == 13 || e == 9) {
                clienteService.obtenerCliente($scope.cliente.Dni).
                    success(function (cliente) {
                        if(cliente != null){
                            notification.warning("El Dni de la persona que desea agregar ya esta ingresado en el sitema.");
                            $scope.cliente.Dni = "";
                        }else{
                            notification.success("El DNI no se encuentra cargado, puede proceder.");
                        }
                    }).
                    error(function (e) {
                        notification.error(e);
                    });
            }
        }
        
        $scope.agregarCliente = function () {
             clienteService.agregarCliente($scope.cliente).
                success(function () {
                    notification.success("Se agrego el cliente con exito");
                    $scope.cargarClientes();
                    $scope.setearModelo();
                }).
                error(function (e) {
                    notification.error("Error al agregar el cliente"); 
                });
        }
        
        //edicion
        $scope.abrirModalModificarCliente =  function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/clientes/modificacionclientes.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                                
                                                $uibModal.open(confirmacionModales).result
                                                        .then(function () {
                                                            $scope.modificarCliente($scope.cliente);
                                                        }, function () {
                                                            $scope.abrirModalModificarCliente();
                                                        });                                              
                                                    }, function () {
                                                        //cancelar 
                                                    });
        }
        
        $scope.modificarCliente = function () {
            clienteService.modificarCliente($scope.cliente).
                success(function () {
                    notification.success("Se modifico el cliente con exito");
                    $scope.setearModelo(); 
                }).
                error(function (e) {
                    notification.error("Error al modificar el cliente"); 
                });
        }
        
        
        //Inicio
        $scope.cargarClientes();
        
        $scope.setearModelo = function () {
            $scope.cliente = {
                Id: 0,
                Dni: "",
                Apellido: "",
                Nombre: "",
                Telefono: "",
                Email: "",
                Direccion: ""
            };
        };
    });