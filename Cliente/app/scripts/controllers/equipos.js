'use strict';

/**
 * @ngdoc function
 * @name clienteApp.controller:EquiposCtrl
 * @description
 * # EquiposCtrl
 * Controller of the clienteApp
 */
angular.module('clienteApp')
    .controller('EquiposCtrl', function ($scope, $window, $uibModal, commonService, uiGridConstants, opcionesGrillaMisEquipos, configuracionFechas,
        confirmacionNuevoEquipoOpcionesModal, confirmacionModales, notification, equipoService, modeloService) {
        $scope.innerHeigth = commonService.getGridInnerHeigth($window.innerHeight);
        $scope.opcionesGrillaMisEquipos = opcionesGrillaMisEquipos;
        
        $scope.equipos = null;
        $scope.equipo;
        $scope.descripcion=null;
        $scope.habilitarEdicion = true;
        
        $scope.abrirModalAltaEquipo = function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/moviles/altaequipos.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                                $uibModal.open(confirmacionNuevoEquipoOpcionesModal).result
                                                        .then(function () {
                                                            console.log($scope.equipo);    
                                                            $scope.altaEquipo($scope.equipo);
                                                                 
                                                        }, function () {
                                                            $scope.abrirModalAltaEquipo();
                                                        });                                              
                                                    }, function () {
                                                    });
        }
        
        $scope.buscarModelo = function () {
            //llamo a servicio que devuelva un modelo para un codigo y lo asigno a $scope.equipo.modelo
            modeloService.buscarModelo($scope.equipo.Modelo.Codigo).
                success(function (modelo) {
                    if(modelo != null){
                        modelo.codigo = $scope.equipo.Modelo.Codigo;
                        $scope.equipo.Modelo = modelo;
                        $scope.descripcion = $scope.equipo.Modelo.Marca.Nombre + "-" + $scope.equipo.Modelo.Nombre + "(" + $scope.equipo.Modelo.Color +"). Caracteristicas: " + $scope.equipo.Modelo.Capacidad + ".";
                    }else{
                        var codigoErroneo = $scope.equipo.Modelo.Codigo;
                        $scope.equipo.Modelo.Codigo = "";
                        notification.warning("El codigo '" + codigoErroneo + "' no se encuentra cargado"); 
                    }
                }).
                error(function (e) {
                    notification.error("Error"); 
                });
        }
        
        $scope.cargarEquipos = function () {
            equipoService.obtenerEquipos().
                success(function (equipos) {
                    $scope.equipos = equipos;
                }).
                error(function (e) {
                    notification.error("Error"); 
                });
        }
        
        $scope.altaEquipo = function () {
            equipoService.agregarEquipo($scope.equipo).
                success(function () {
                    notification.success("Se dio de alta al equipo con exito");
                    $scope.setearEquipo(); 
                    $scope.cargarEquipos();
                }).
                error(function (e) {
                    notification.error("Error al dar de alta el equipo"); 
                });
        }
        
        $scope.modificarEquipo = function () {
            equipoService.modificarEquipo($scope.equipo).
                success(function () {
                    notification.success("Se modifico el equipo con exito");
                    $scope.cargarEquipos();
                    //$scope.setearEquipo(); 
                }).
                error(function (e) {
                    notification.error("Error al modificar el equipo"); 
                });
        }
        
        $scope.abrirModalModificarEquipo =  function(){
             $uibModal.open({animation: true,
                            size: 'lg',
                            templateUrl: 'views/moviles/modificacionequipo.html',
                            controller: 'ModaloptionsCtrl',
                            scope: $scope}).result.then(function () {
                                                
                                                $uibModal.open(confirmacionModales).result
                                                        .then(function () {
                                                            $scope.modificarEquipo($scope.equipo);
                                                        }, function () {
                                                            $scope.abrirModalModificarEquipo();
                                                        });                                              
                                                    }, function () {
                                                        //cancelar 
                                                    });
          $scope.buscarModelo();
        }
        
        $scope.eliminarEquipo = function () {
            $uibModal.open(confirmacionModales).result
                    .then(function () {                        
                        equipoService.eliminarEquipo($scope.equipo.Imei).
                                    success(function () {
                                        notification.success("Se elimino el equipo con exito");
                                    }).
                                    error(function (e) {
                                        notification.error("Error al eliminar el equipo"); 
                                    });
                        
                    }, function () {
                        //Cancelar
                    });                                              
        }
        
        $scope.opcionesGrillaMisEquipos.onRegisterApi = function(opcionesGrillaMisEquipos){
             opcionesGrillaMisEquipos.selection.on.rowSelectionChanged($scope,function(row){
                 $scope.habilitarEdicion = false;
                $scope.equipo =  row.entity;
            });
        };
        
        $scope.setearEquipo =function() {
           $scope.equipo={
            Imei:"",
            Precio:"",
            Descripcion:"",
            Modelo:{
                Codigo:""
            }} 
        };
        //Inicio
        
        $scope.cargarEquipos();
        $scope.setearEquipo();
    });