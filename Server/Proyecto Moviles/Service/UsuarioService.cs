﻿using System;
using Datos.DAO;
using Model;
using Model.DTOs;
using static System.String;

namespace Service
{
    public interface IUsuarioService : IAutorizacionService
    {
        UsuarioDto Login(Usuario user);
        UsuarioDto Login(Guid token);
        void LogOut(Guid token);
    }

    public interface IAutorizacionService
    {
        bool EstaAutorizado(Guid token);
    }

    public class UsuarioService : IUsuarioService
    {
        private readonly UsuarioDao _usuarioDao;
        public UsuarioService(UsuarioDao usuarioDao)
        {
            _usuarioDao = usuarioDao;
        }

        public UsuarioDto Login(Usuario user)
        {
            if (user == null)
                throw  new Exception("Contactar al soporte. Error del sistema. LOG: Login method at Service.");
            if (IsNullOrWhiteSpace(user.UserName))
                throw new Exception("El nombre de usuario no puede ser vacio");
            var usr = _usuarioDao.BuscarPorNombreDeUsuario(user.UserName);
            if (usr == null)
                throw new Exception("El usuario no se encuentra en la base de datos");
            if (user.Password != usr.Password)
                throw new Exception("Password incorrecto");
            usr.Token = Guid.NewGuid();
            usr.ExpireDate = DateTime.Now.AddDays(1);
            _usuarioDao.GuardarOActualizar(usr);

            return ToDto(usr);
        }

        public UsuarioDto Login(Guid token)
        {
            var usr =  _usuarioDao.BuscarPorToken(token);
            return usr != null ? ToDto(usr) : new UsuarioDto();
        }

        public void LogOut(Guid token)
        {
            throw new NotImplementedException();
        }

        public bool EstaAutorizado(Guid token)
        {
            return _usuarioDao.BuscarPorToken(token)!=null;
        }

        private UsuarioDto ToDto(Usuario usr)
        {
            return new UsuarioDto
            {
                Token = usr.Token.Value,
                UserName = usr.UserName,
                Password = usr.Password,
                Nombre = usr.Nombre + " " + usr.Apellido,
                ExpirationDate = usr.ExpireDate,
                Rol = usr.Rol == RolEnum.Administrador ? RolEnum.Administrador : RolEnum.Vendedor
            };
        }
    }
}