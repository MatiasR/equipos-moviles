﻿using Datos.DAO;
using Model;

namespace Service
{
    public interface IVentaService
    {
        void AgregarVenta(Venta venta);
        
    }
    public class VentaService : IVentaService
    {
        private readonly VentaDao _ventaDao;
        public VentaService(VentaDao ventaDao)
        {
            _ventaDao = ventaDao;
        }

        public void AgregarVenta(Venta venta)
        {
            _ventaDao.Agregar(venta);
        }

    }
}