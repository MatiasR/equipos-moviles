﻿using System.Collections.Generic;
using Datos.DAO;
using Model;
using Model.DTOs;

namespace Service
{
    public interface IEquipoService
    {
        List<EquipoDto> ObtenerEquipos();
        void AgregarEquipo(Equipo equipo);
        void ModificarEquipo(Equipo equipo);
    }
    public class EquipoService : IEquipoService
    {
        private IEquipoDao _equipoDao;

        public EquipoService(IEquipoDao equipoDao)
        {
            _equipoDao = equipoDao;
        }

        public List<EquipoDto> ObtenerEquipos()
        {
            var listadoDeEquipos = new List<EquipoDto>();
            listadoDeEquipos = _equipoDao.ObtenerEquipos();

            return listadoDeEquipos;
        }

        public void AgregarEquipo(Equipo equipo)
        {
           _equipoDao.Agregar(equipo);
        }

        public void ModificarEquipo(Equipo equipo)
        {
            _equipoDao.Modificar(equipo);
        }
    }
}