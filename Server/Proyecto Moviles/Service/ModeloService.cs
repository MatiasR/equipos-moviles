﻿using System.Collections.Generic;
using System.Linq;
using Datos.DAO;
using Model;
using Model.DTOs;

namespace Service
{
    public interface IModeloService
    {
        ModeloDto ObtenerModeloPorCodigo(string codigo);
        List<ModeloDto> ObtenerModelos();
        void AgregarModelo(Modelo modelo);
        void ModificarModelo(Modelo modelo);
    }

    public class ModeloService : IModeloService
    {
        private readonly IModeloDao _modeloDao;

        public ModeloService(IModeloDao modeloDao)
        {
            _modeloDao = modeloDao;
        }

        public ModeloDto ObtenerModeloPorCodigo(string codigo)
        {
            return _modeloDao.ObtenerPorCodigo(codigo);
        }

        public List<ModeloDto> ObtenerModelos()
        {
            return _modeloDao.Obtener().ToList();
        }

        public void AgregarModelo(Modelo modelo)
        {
            _modeloDao.Agregar(modelo);
        }

        public void ModificarModelo(Modelo modelo)
        {
            _modeloDao.Modificar(modelo);
        }
    }
}