using System;
using Model;
using NHibernate;

namespace Datos.DAO
{
    public interface IVentaDao : IDao<Venta, Guid>
    {

    }
    public class VentaDao : BaseDao<Venta, Guid>,IVentaDao
    {
        public VentaDao(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }
    }
}