﻿using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using NHibernate;

namespace Datos.DAO
{
    public interface IModeloDao
    {
        IList<ModeloDto> Obtener();
        ModeloDto ObtenerPorCodigo(string codigo);
        void Agregar(Modelo modelo);
        void Modificar(Modelo modelo);
    }

    public class ModeloDao : IModeloDao
    {
        private readonly ISessionFactory _sessionFactory;

        private ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }

        public ModeloDao(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        //Obtener todos los modelos
        public IList<ModeloDto> Obtener()
        {
            var modelos = CurrentSession.QueryOver<Modelo>().List<Modelo>().Select(ModeloToDto).ToList();
            return modelos;
        }

        public ModeloDto ObtenerPorCodigo(string codigo)
        {
            var modeloDto = CurrentSession.QueryOver<Modelo>().Where(c => c.Codigo == codigo).List<Modelo>().Select(ModeloToDto).FirstOrDefault();
            return modeloDto;
        }

        public void Agregar(Modelo modelo)
        {
            CurrentSession.Save(modelo);
        }

        public void Modificar(Modelo modelo)
        {
            CurrentSession.SaveOrUpdate(modelo);
            CurrentSession.Flush();
        }

        private ModeloDto ModeloToDto(Modelo modelo)
        {
            if (modelo != null)
            {
                return new ModeloDto
                {
                    Capacidad = modelo.Capacidad,
                    Codigo = modelo.Codigo,
                    Color = modelo.Color,
                    Descripcion = modelo.Descripcion,
                    Id = modelo.Id,
                    Nombre = modelo.Nombre,
                    Marca = modelo.Marca != null? new Marca { Id = modelo.Marca.Id, Nombre = modelo.Marca.Nombre } : null
                };
            }

            return new ModeloDto();
        }
    }
}