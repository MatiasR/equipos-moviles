using System;
using Model;
using NHibernate;

namespace Datos.DAO
{
    public interface IPagoDao : IDao<Pago, Guid>
    {

    }
    public class PagoDao : BaseDao<Pago,Guid>, IPagoDao
    {
        public PagoDao(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }
    }
}