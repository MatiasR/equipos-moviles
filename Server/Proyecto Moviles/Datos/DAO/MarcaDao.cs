﻿using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using NHibernate;

namespace Datos.DAO
{
    public interface IMarcaDao
    {
        IList<MarcaDto> Obtener();
    }

    public class MarcaDao : IMarcaDao
    {
        private readonly ISessionFactory _sessionFactory;

        public MarcaDao(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        private ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }

        //Obtener todas las marcas
        public IList<MarcaDto> Obtener()
        {
            var marcas = CurrentSession.QueryOver<Marca>().List<Marca>().Select(MarcaToDto).ToList();
            return marcas;
        }

        private MarcaDto MarcaToDto(Marca marca)
        {
            if (marca != null)
            {
                return new MarcaDto
                {
                    Id = marca.Id,
                    Nombre = marca.Nombre,
                };
            }

            return new MarcaDto();
        }
    }
}