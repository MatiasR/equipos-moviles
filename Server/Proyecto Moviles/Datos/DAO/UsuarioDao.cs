using System;
using Model;
using NHibernate;

namespace Datos.DAO
{

    public interface IUsuarioDao : IDao<Usuario, Guid>
    {
    }
    public class UsuarioDao : BaseDao<Usuario, Guid>, IUsuarioDao
    {
        public UsuarioDao(ISessionFactory sessionFactory) : base(sessionFactory)
        {
        }

        public Usuario BuscarPorNombreDeUsuario(string userName)
        {
            return CurrentSession.QueryOver<Usuario>().Where(x => x.UserName == userName).SingleOrDefault();
        }

        public Usuario BuscarPorToken(Guid token)
        {
            using ( var sesion = _sessionFactory.OpenSession())
            {
                return sesion.QueryOver<Usuario>().Where(x => x.Token == token).SingleOrDefault();
            }
        }
    }
}