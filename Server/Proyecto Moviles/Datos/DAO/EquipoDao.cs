using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using NHibernate;

namespace Datos.DAO
{
    public interface IEquipoDao
    {
        List<EquipoDto> ObtenerEquipos();
        void Agregar(Equipo equipo);
        void Modificar(Equipo equipo);
    }

    public class EquipoDao : IEquipoDao
    {
        private readonly ISessionFactory _sessionFactory;

        private ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }

        public EquipoDao(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public List<EquipoDto> ObtenerEquipos()
        {
            var equipos = CurrentSession.QueryOver<Equipo>().List<Equipo>().Select(EquipoToDto).ToList();
            return equipos;
        }

        public void Agregar(Equipo equipo)
        {
            CurrentSession.Save(equipo);
        }

        public void Modificar(Equipo equipo)
        {
            CurrentSession.SaveOrUpdate(equipo);
            CurrentSession.Flush();
        }

        private EquipoDto EquipoToDto(Equipo equipo)
        {
            if (equipo != null)
            {
                return new EquipoDto
                {
                    Descripcion = equipo.Descripcion,
                    Id = equipo.Id,
                    Imei = equipo.Imei,
                    Precio = equipo.Precio,
                    Modelo = equipo.Modelo != null ? new Modelo
                    {
                        Id = equipo.Id,
                        Nombre = equipo.Modelo.Nombre,
                        Capacidad = equipo.Modelo.Capacidad,
                        Codigo = equipo.Modelo.Codigo,
                        Color = equipo.Modelo.Color,
                        Descripcion = equipo.Modelo.Descripcion,
                        Marca = new Marca { Nombre = equipo.Modelo.Marca.Nombre, Id = equipo.Modelo.Marca.Id}
                    } : null
                };
            }
            return new EquipoDto();
        }
    }
}