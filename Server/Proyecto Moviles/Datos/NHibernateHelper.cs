﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Model;
using NHibernate;
using NHibernate.Dialect;
using NHibernate.Tool.hbm2ddl;

namespace Datos
{
    public static class NHibernateHelper
    {
        private static ISessionFactory _sessionFactory;

        private static ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                    InitializeSessionFactory();
                return _sessionFactory;
            }
        }

        private static void InitializeSessionFactory()
        {
            _sessionFactory = Fluently.Configure()
                .Database(
                    MsSqlCeConfiguration.MsSqlCe40.ConnectionString(
                        @"Server=localhost\MSI\SQLEXPRESS;Database=Telefonia;TrustedConnection=True;")
                        .ShowSql())
                .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Cliente>())
                .ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true, true))
                .BuildSessionFactory();

        }

        public static ISession OpenSession()
        {
            return SessionFactory.OpenSession();
        }
    }
}