﻿using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class MarcaMapper : ClassMapping<Marca>
    {
        public MarcaMapper()
        {
            Id(x => x.Id, f => f.Generator(Generators.GuidComb));
            Property(x => x.Nombre);

            Bag<Modelo>(x => x.Modelos, c => { c.Inverse(true); }, r => { r.OneToMany(); });
        }
    }
}