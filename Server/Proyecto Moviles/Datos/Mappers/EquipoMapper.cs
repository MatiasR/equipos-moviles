﻿using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class EquipoMapper : ClassMapping<Equipo>
    {
        public EquipoMapper()
        {
            Id(x => x.Id, mapper => mapper.Generator(Generators.GuidComb));
            Property(x => x.Imei);
            Property(x => x.Descripcion);
            Property(x => x.Precio);

            ManyToOne<Modelo>(x => x.Modelo, c => { c.Cascade(Cascade.Persist); });
            ManyToOne<Venta>(x => x.Venta, c => { c.Cascade(Cascade.Persist); });
        }
    }
}