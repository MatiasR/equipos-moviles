using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class PagoMapper : ClassMapping<Pago>
    {
        public PagoMapper()
        {
            Id(x => x.Id, mapper => mapper.Generator(Generators.GuidComb));
            Property(x => x.Fecha);
            Property(x => x.Monto);
        }
    }
}