﻿using System;
using System.Web.Http;
using System.Web.Http.Cors;
using Autofac;
using Autofac.Integration.WebApi;
using Datos.DAO;
using NHibernate;
using Datos.Mappers;
using Model;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using Proyecto_Moviles.Controllers;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //AUTOFAC: El ContainerBuilder registra todas las clases que quiero instanciar.
            var container = new ContainerBuilder();
            var cfg = new NHibernate.Cfg.Configuration();
            //Obtengo la session factory. Hay que tener el dialecto y un driver para la BD configurado. Luego setear el connection String
            cfg.DataBaseIntegration(x =>
            {
                x.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                x.Driver<NHibernate.Driver.SqlClientDriver>();
                x.ConnectionString = "Data Source=(local)\\sqlexpress;Database=Telefonia;Trusted_Connection=True;";
                //x.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote; Permite mappear palabras claves como por ejemplo Date. Depende de la BD usada.
                x.LogSqlInConsole = true;
                x.LogFormattedSql = true;
            }
            );
            //se tiene que setear el contexto en el cual va a operar(una sesion un thread)
            cfg.CurrentSessionContext<NHibernate.Context.WebSessionContext>(); 
            var modelMapper = new ModelMapper();
            modelMapper.AddMapping(typeof(ClienteMapper));
            modelMapper.AddMapping(typeof(EquipoMapper));
            modelMapper.AddMapping(typeof(ModeloMapper));
            modelMapper.AddMapping(typeof(MarcaMapper));
            modelMapper.AddMapping(typeof(UsuarioMapper));
            modelMapper.AddMapping(typeof(VentaMapper));
            var mapping = modelMapper.CompileMappingForAllExplicitlyAddedEntities();
            cfg.AddMapping(mapping);
            cfg.BuildMapping();

            //primer parametro mostrar en consola. Segundo lo ejecuta o no. El schemaExport hace un drop de la tabla si existe y la vuelve a crear
            //var schema = new SchemaExport(cfg);
            //schema.Create(true, true);
            //El SchemaUpdate hace un update de la tabla en caso de que haya agregado una propiedad mas en el modelo, lo que se traduce en una col mas en la bd.
            var schemaUpdater = new SchemaUpdate(cfg);
            schemaUpdater.Execute(true, true);

            //AUTOFAC: Configuracion particular de lo que necesite.Registro session factory
            var sessionFactory = cfg.BuildSessionFactory();
            container.RegisterInstance(sessionFactory).As<ISessionFactory>().SingleInstance();
            //

            //Configuracion DAOS
            container.Register(x => new ClienteDao(x.Resolve<ISessionFactory>())).SingleInstance();
            container.Register(x => new ModeloDao(x.Resolve<ISessionFactory>())).SingleInstance();
            container.Register(x => new MarcaDao(x.Resolve<ISessionFactory>())).SingleInstance();
            container.Register(x => new EquipoDao(x.Resolve<ISessionFactory>())).SingleInstance();
            container.Register(x => new VentaDao(x.Resolve<ISessionFactory>())).SingleInstance();

            container.Register(x => new UsuarioDao(x.Resolve<ISessionFactory>())).SingleInstance();
            //END - Configuracion DAOS

            //Configuracion Servicios
            container.Register(x => new ModeloService(x.Resolve<ModeloDao>())).As<IModeloService>().SingleInstance();
            container.Register(x => new MarcaService(x.Resolve<MarcaDao>())).As<IMarcaService>().SingleInstance();
            container.Register(x => new EquipoService(x.Resolve<EquipoDao>())).As<IEquipoService>().SingleInstance();
            container.Register(x => new ClienteService(x.Resolve<ClienteDao>())).As<IClienteService>().SingleInstance();
            container.Register(x => new VentaService(x.Resolve<VentaDao>())).As<IVentaService>().SingleInstance();

            container.Register(x => new UsuarioService(x.Resolve<UsuarioDao>())).As<IUsuarioService>().SingleInstance();

            //END - Configuracion Servicios

            //Configuracion Controllers
            container.Register(x => new ClientesController(x.Resolve<IClienteService>())).InstancePerRequest();
            container.Register(c => new ModelosController(c.Resolve<IModeloService>())).InstancePerRequest();
            container.Register(c =>  new MarcasController(c.Resolve<IMarcaService>())).InstancePerRequest();
            container.Register(c => new EquiposController(c.Resolve<IEquipoService>())).InstancePerRequest();
            container.Register(c => new VentasController(c.Resolve<IVentaService>())).InstancePerRequest();

            container.Register(c => new UsuariosController(c.Resolve<IUsuarioService>())).InstancePerRequest();

            //END - Configuracion Controllers

            container.RegisterType<MarcasController>().PropertiesAutowired().InstancePerRequest();
            container.RegisterType<UsuarioService>().PropertiesAutowired().InstancePerRequest();

            

            container.RegisterWebApiFilterProvider(config);
            container.Register(x => new SessionProviderAttribute(x.Resolve<ISessionFactory>())).InstancePerRequest();
            container.Register(x => new AutorizacionAttribute(x.Resolve<IUsuarioService>())).InstancePerRequest();

            var iContainer = container.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(iContainer);

            //END AUTOFAC Configuration
            // Web API routes
            config.MapHttpAttributeRoutes();
            var corsAttr = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(corsAttr);
        }

        private static void ConfigureSessionFactory(ContainerBuilder container)
        {
            //var settings = NHibernate.Create();
        }
    }
}
