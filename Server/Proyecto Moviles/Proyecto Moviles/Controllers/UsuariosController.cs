﻿using System;
using System.Web.Http;
using Model;
using Model.DTOs;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class UsuariosController : ApiController
    {
        private readonly IUsuarioService _usuarioService;

        public UsuariosController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;
        }

        [HttpPost, Route("api/login"), SessionProvider]
        public UsuarioDto Login([FromBody]Usuario usuario)
        {
            return _usuarioService.Login(usuario);
        }

        [HttpPost, Route("api/login/{token}"), SessionProvider]
        public UsuarioDto Login(Guid token)
        {
            return _usuarioService.Login(token);
        }

        [HttpPost, Route("api/logOut"), SessionProvider()]
        public void  Logout (Guid token)
        {
             _usuarioService.LogOut(token);
        }
    }
}