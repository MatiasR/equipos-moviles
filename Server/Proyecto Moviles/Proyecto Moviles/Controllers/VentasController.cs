using System.Web.Http;
using Model;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class VentasController : ApiController
    {
        private readonly IVentaService _ventaService;

        public VentasController(IVentaService ventaService)
        {
            _ventaService = ventaService;
        }

    
        //agrego el cliente
        [HttpPost, Route("api/venta"), SessionProvider(ReadOnly = false)]
        public void AgregarVenta([FromBody]Venta venta)
        {
            _ventaService.AgregarVenta(venta);
        }
    }
}