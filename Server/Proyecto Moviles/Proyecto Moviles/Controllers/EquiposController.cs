﻿using System.Collections.Generic;
using System.Web.Http;
using Model;
using Model.DTOs;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class EquiposController : ApiController
    {
        private readonly IEquipoService _equipoService;

        public EquiposController(IEquipoService equipoService)
        {
            _equipoService = equipoService;
        }

        [HttpGet, Route("api/equipos"), SessionProvider(ReadOnly = true), Autorizacion ]
        public List<EquipoDto> ObtenerEquipos()
        {
            return _equipoService.ObtenerEquipos();
        }

        //agrego el equipo 
        [HttpPost, Route("api/equipos"), SessionProvider(ReadOnly = false)]
        public void AgregarEquipo([FromBody]Equipo equipo)
        {
            _equipoService.AgregarEquipo(equipo);
        }

        [HttpPut, Route("api/equipo"), SessionProvider(ReadOnly = false)]
        public void ModificarEquipo([FromBody]Equipo equipo)
        {
            _equipoService.ModificarEquipo(equipo);
        }

        [HttpDelete, Route("api/equipo/{imei}")]
        public void EliminarEquipo(string imei)
        {
            //agrego el equipo
        }
    }
}