﻿using System.Collections.Generic;
using System.Web.Http;
using Model.DTOs;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class MarcasController : ApiController
    {
        private readonly IMarcaService _marcaService;

        public MarcasController(IMarcaService marcaService)
        {
            _marcaService = marcaService;
        }

        [HttpGet, Route("api/marcas"), SessionProvider(ReadOnly = true)]
        public IList<MarcaDto> ObtenerMarcas()
        {
            return _marcaService.ObtenerMarcas();
        }
    }
}
