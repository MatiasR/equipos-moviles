﻿using System.Collections.Generic;
using System.Web.Http;
using Model;
using Model.DTOs;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class ClientesController : ApiController
    {
        private readonly IClienteService _clienteService;

        public ClientesController(IClienteService clienteService)
        {
            _clienteService = clienteService;
        }

        [HttpGet, Route("api/clientes"), SessionProvider(ReadOnly = true)]
        public IList<ClienteDto> ObtenerClientes()
        {
            return _clienteService.ObtenerClientes();
        }

        [HttpGet, Route("api/clientes/{dni}"), SessionProvider(ReadOnly = true)]
        public ClienteDto ObtenerCliente(string dni)
        {
            return _clienteService.ObtenerCliente(dni);
        }

        //agrego el cliente
        [HttpPost, Route("api/cliente"), SessionProvider(ReadOnly = false)]
        public void AgregarCliente([FromBody]Cliente cliente)
        {
            _clienteService.Agregarcliente(cliente);
        }

        //modifico el cliente
        [HttpPut, Route("api/cliente"), SessionProvider(ReadOnly = false)]
        public void ModificarCliente([FromBody]Cliente cliente)
        {
            _clienteService.ModificarCliente(cliente);
        }
    }
}
