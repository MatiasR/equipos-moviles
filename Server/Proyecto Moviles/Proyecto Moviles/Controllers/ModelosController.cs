﻿using System.Collections.Generic;
using System.Web.Http;
using Model;
using Model.DTOs;
using Proyecto_Moviles.Providers;
using Service;

namespace Proyecto_Moviles.Controllers
{
    public class ModelosController : ApiController
    {
        private readonly IModeloService _modeloService;

        public ModelosController(IModeloService modeloService)
        {
            _modeloService = modeloService;
        }

        // GET: Modelos
        [HttpGet, Route("api/modelo/{codigo}"), SessionProvider(ReadOnly = true)]
        public ModeloDto ObtenerModelo(string codigo)
        {
            return _modeloService.ObtenerModeloPorCodigo(codigo);
        }

        [HttpGet, Route("api/modelos/"), SessionProvider(ReadOnly = true)]
        public List<ModeloDto> ObtenerModelos()
        {
            return _modeloService.ObtenerModelos();
        }

        //agrego el modelo
        [HttpPost, Route("api/modelo"), SessionProvider(ReadOnly = false)]
        public void AgregarModelo([FromBody]Modelo modelo)
        {
            _modeloService.AgregarModelo(modelo);
        }

        //modifico el modelo
        [HttpPut, Route("api/modelo"), SessionProvider(ReadOnly = false)]
        public void ModificarModelo([FromBody]Modelo modelo)
        {
            _modeloService.ModificarModelo(modelo);
        }
    }
}