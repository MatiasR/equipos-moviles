using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Service;

namespace Proyecto_Moviles.Providers
{
    public class AutorizacionAttribute: AuthorizationFilterAttribute, Autofac.Integration.WebApi.IAutofacAuthorizationFilter
    {
       // private readonly IAutorizacionService _service;
        public IUsuarioService _service { get; set; }
        public AutorizacionAttribute(IUsuarioService service)
        {
            _service = service;
        }

        internal AutorizacionAttribute()
        {
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var tokenGuid = Guid.Empty;
            var token = actionContext.Request.Headers.GetValues("Authorization").ElementAt(0);
            tokenGuid = new Guid(Convert.ToString(token));
            if (string.IsNullOrWhiteSpace(token))
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized) {ReasonPhrase = "Logueate"});
            if (!_service.EstaAutorizado(tokenGuid))
            throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = "Lo sentimos pero no te vamos a dejar entrar porque no estas autorizado" });
        }
    }
}