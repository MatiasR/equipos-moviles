﻿using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Autofac.Integration.WebApi;
using NHibernate;
using NHibernate.Context;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;

namespace Proyecto_Moviles.Providers
{
    public class SessionProviderAttribute : ActionFilterAttribute, IAutofacActionFilter
    {
        public ISessionFactory SessionFactory { get; set; }
        public bool ReadOnly { get; set; }

        internal SessionProviderAttribute()
        {
        }

        public SessionProviderAttribute(ISessionFactory sessionFactory)
        {
            this.SessionFactory = sessionFactory;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            //Se termino de ejecutar
            var session = SessionFactory.GetCurrentSession();
            var transaction = session.Transaction;
            if (transaction != null && transaction.IsActive)
            {
                transaction.Commit();
            }

            session = CurrentSessionContext.Unbind(SessionFactory);
            session.Close();
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //Se esta ejecutando
            var session = SessionFactory.OpenSession();
            session.DefaultReadOnly = ReadOnly;
            session.FlushMode = ReadOnly ? FlushMode.Never : FlushMode.Auto;
            CurrentSessionContext.Bind(session);
            session.BeginTransaction();
        }
    }
}