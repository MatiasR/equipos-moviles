using Moq;
using NHibernate;
using NUnit.Framework;
using Proyecto_Moviles.Providers;

namespace WebTest
{
    public class SessionProviderAttributeTest
    {
        [Test]
        public void Test()
        {
            var session = new Mock<ISession>(MockBehavior.Strict);
            session.Setup(x => x.DefaultReadOnly).Returns(false);
            //session.Setup(x => x.FlushMode).Returns(FlushMode.Auto);
            var sessionFactory = new Mock<ISessionFactory>(MockBehavior.Strict);

            sessionFactory.Setup(s => s.OpenSession()).Returns(session.Object);
            var atr = new SessionProviderAttribute(sessionFactory.Object);
            sessionFactory.Setup(x => x.GetCurrentSession()).Returns(session.Object);
            
            //
            //atr.OnActionExecuting(null);
            //
            //sessionFactory.VerifyAll();
        }
    }
}