﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using Moq;
using NUnit.Framework;
using Proyecto_Moviles.Controllers;
using Service;

namespace WebTest
{
    public class ClientesControllerTest
    {
        private ClientesController _clientesController;
        private Mock<IClienteService> _clienteServiceMock;
        private List<ClienteDto> _clientes;

        [SetUp]
        public void Init()
        {
            _clienteServiceMock = new Mock<IClienteService>();
            _clientes = RetornarClientesDto();
            _clienteServiceMock.Setup(x => x.ObtenerClientes()).Returns(_clientes);
            _clienteServiceMock.Setup(x => x.ObtenerCliente(It.IsAny<string>())).Returns(_clientes.ElementAt(0));
            _clienteServiceMock.Setup(x => x.Agregarcliente(It.IsAny<Cliente>()));
            _clienteServiceMock.Setup(x => x.ModificarCliente(It.IsAny<Cliente>()));
            _clientesController = new ClientesController(_clienteServiceMock.Object);
        }

        [Test]
        public void DevuelvoUnaListaDeClientesCuandoLlamoAlServicio()
        {
            var result = _clientesController.ObtenerClientes();
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result.ElementAt(0).Apellido, _clientes.ElementAt(0).Apellido);
            Assert.AreEqual(result.ElementAt(1).Apellido, _clientes.ElementAt(1).Apellido);
        }

        [Test]
        public void DevuelvoUnClienteCuandoBuscoPorDni()
        {
            var result = _clientesController.ObtenerCliente("30648983");
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Dni, _clientes.ElementAt(0).Dni);
        }

        [Test]
        public void SeDebeLlamarAlServicioUnaVezCuandoSeAgregaUnCliente()
        {
            _clientesController.AgregarCliente(RetornarClientes().ElementAt(0));
            _clienteServiceMock.Verify(x => x.Agregarcliente(It.IsAny<Cliente>()), Times.Once);
        }

        [Test]
        public void SeDebeLlamarAlServicioUnaVezCuandoSeModificaUnCliente()
        {
            _clientesController.ModificarCliente(RetornarClientes().ElementAt(0));
            _clienteServiceMock.Verify(x => x.ModificarCliente(It.IsAny<Cliente>()), Times.Once);
        }

        private List<ClienteDto> RetornarClientesDto()
        {
            return new List<ClienteDto>
            {
                new ClienteDto {Nombre = "Matias", Apellido = "Raverta", Id = Guid.NewGuid(), Date = DateTime.Now.ToLongDateString(), Direccion = "Calle 8", Dni = "30648983", Email = "milodude023@gmail.com", Telefono = "4642869"},
                new ClienteDto {Nombre = "Matias", Apellido = "Memolli", Id = Guid.NewGuid(), Date = DateTime.Now.ToLongDateString(), Direccion = "Calle 9", Dni = "35648984", Email = "mmemolli@gmail.com", Telefono = "4642861"}
            };
        }

        private List<Cliente> RetornarClientes()
        {
            return new List<Cliente>
            {
                new Cliente {Nombre = "Matias", Apellido = "Raverta", Id = Guid.NewGuid(), Date = DateTime.Now.ToLongDateString(), Direccion = "Calle 8", Dni = "30648983", Email = "milodude023@gmail.com", Telefono = "4642869"},
                new Cliente {Nombre = "Matias", Apellido = "Memolli", Id = Guid.NewGuid(), Date = DateTime.Now.ToLongDateString(), Direccion = "Calle 9", Dni = "35648984", Email = "mmemolli@gmail.com", Telefono = "4642861"}
            };
        }
    }
}
