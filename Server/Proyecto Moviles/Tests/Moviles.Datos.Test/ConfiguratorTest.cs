﻿using Datos.Mappers;
using Model;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace DatosTest
{
    public class ConfiguratorTest
    {
        //Nhibernate tiene clases propias que son las que se utilizan para configurar. Configurator, SessionFactory, Session, etc.
        [Test, Ignore("Never Worked")]
        public void VeryFirstTest()
        {
            var config = new Configuration();
            //Obtengo la session factory. Hay que tener el dialecto y un driver para la BD configurado. Luego setear el connection String
            config.DataBaseIntegration(x =>
            {
                x.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                x.Driver<NHibernate.Driver.SqlClientDriver>();
                x.ConnectionString = "Data Source=(local)\\sqlexpress;Database=Telefonia;Trusted_Connection=True;";
                //x.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote; Permite mappear palabras claves como por ejemplo Date. Depende de la BD usada.
                x.LogSqlInConsole = true;
                x.LogFormattedSql = true;
            }
            );
           // config.CurrentSessionContext<NHibernate.Context.WebSessionContext>(); se tiene que setear el contexto en el cual va a operar(una sesion un thread)
            var modelMapper = new ModelMapper();
            modelMapper.AddMapping(typeof(ClienteMapper));
            var mapping = modelMapper.CompileMappingForAllExplicitlyAddedEntities();
            config.AddMapping(mapping);
            config.BuildMapping();

            //primer parametro mostrar en consola. Segundo lo ejecuta o no. El schemaExport hace un drop de la tabla si existe y la vuelve a crear
            var schema = new SchemaExport(config);
            schema.Create(true, true);
            //El SchemaUpdate hace un update de la tabla en caso de que haya agregado una propiedad mas en el modelo, lo que se traduce en una col mas en la bd.
            //var schemaUpdater = new SchemaUpdate(config);
            //schemaUpdater.Execute(true, true);
            
            var sessionFactory = config.BuildSessionFactory();
            Assert.IsNotNull(sessionFactory);
            
            //Siempre es recomendable usar la session con transacciones. Esto es para aislar la sesion. WebApi crea una sesion por cada requerimiento.
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                var cliente = new Cliente
                {
                    Nombre = "Nombre",
                    Apellido = "Apellido1",
                    Date = "test"
                };

                session.Save(cliente);
                transaction.Commit();
            }
        }
    }
}
