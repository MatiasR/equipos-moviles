﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.Mappers;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace DatosTest
{
    public class ConfigurationHibernateTest
    {
        
        public ISessionFactory InitConfig()
        {
            var config = new Configuration();
            //Obtengo la session factory. Hay que tener el dialecto y un driver para la BD configurado. Luego setear el connection String
            config.DataBaseIntegration(x =>
            {
                x.Dialect<NHibernate.Dialect.MsSql2008Dialect>();
                x.Driver<NHibernate.Driver.SqlClientDriver>();
                x.ConnectionString = "Data Source=(local)\\sqlexpress;Database=TelefoniaTest;Trusted_Connection=True;";
                //x.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote; Permite mappear palabras claves como por ejemplo Date. Depende de la BD usada.
                x.LogSqlInConsole = true;
                x.LogFormattedSql = true;
            }
            );
            // config.CurrentSessionContext<NHibernate.Context.WebSessionContext>(); se tiene que setear el contexto en el cual va a operar(una sesion un thread)
            var modelMapper = new ModelMapper();
            modelMapper.AddMapping(typeof(ClienteMapper));
            modelMapper.AddMapping(typeof(ModeloMapper));
            modelMapper.AddMapping(typeof(MarcaMapper));
            modelMapper.AddMapping(typeof(EquipoMapper));
            modelMapper.AddMapping(typeof(VentaMapper));
            var mapping = modelMapper.CompileMappingForAllExplicitlyAddedEntities();
            config.AddMapping(mapping);
            config.BuildMapping();

            //primer parametro mostrar en consola. Segundo lo ejecuta o no. El schemaExport hace un drop de la tabla si existe y la vuelve a crear
            var schema = new SchemaExport(config);
            schema.Create(true, true);
            //El SchemaUpdate hace un update de la tabla en caso de que haya agregado una propiedad mas en el modelo, lo que se traduce en una col mas en la bd.
            //var schemaUpdater = new SchemaUpdate(config);
            //schemaUpdater.Execute(true, true);
            return config.BuildSessionFactory();
        }
    }
}
