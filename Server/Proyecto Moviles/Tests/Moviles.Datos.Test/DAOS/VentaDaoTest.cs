﻿using System;
using System.Collections.Generic;
using Datos.Mappers;
using Model;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NUnit.Framework;

namespace DatosTest.DAOS
{
    public class VentaDaoTest : ConfigurationHibernateTest
    {
        public ISessionFactory SessionFactory;

        [SetUp]
        public void Initialize()
        {
            
        }

        [Test, Ignore("Not Tested")]
        public void VeryFirstTest()
        {
            
            var sessionFactory = InitConfig();
            Assert.IsNotNull(sessionFactory);

            //Siempre es recomendable usar la session con transacciones. Esto es para aislar la sesion. WebApi crea una sesion por cada requerimiento.
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                var cliente = new Cliente
                {
                    Nombre = "Nombre",
                    Apellido = "Apellido1",
                    Date = "test"
                };
                var marca = new Marca
                {
                    Nombre = "Apple"
                };
                var modelo = new Modelo
                {
                    Capacidad = "16 gb",
                    Codigo = "3388",
                    Marca = marca
                };
                var equipo = new Equipo
                {
                    Modelo = modelo,
                    Descripcion = "descripcion",
                    Imei = "Imai",
                    Precio = 100035
                };
                var Venta = new Venta
                {
                    CantidadCuotas = 0,
                    Cliente = cliente,
                    Fecha = DateTime.Now,
                    Precio = 20000,
                    Equipos = new List<Equipo>()
                };
                
                marca.Modelos.Add(modelo);
                modelo.Equipos.Add(equipo);
                Venta.Equipos.Add(equipo);
                session.Save(cliente);
                session.Save(marca);
                session.Save(modelo);
                transaction.Commit();
                
            }
        }
    }
}
