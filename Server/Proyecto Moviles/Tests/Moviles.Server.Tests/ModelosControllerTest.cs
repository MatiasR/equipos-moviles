﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using Moq;
using NUnit.Framework;
using Proyecto_Moviles.Controllers;
using Service;

namespace WebTest
{
    public class ModelosControllerTest
    {
        ModelosController _modelosController;
        private Mock<IModeloService> _modeloServiceMock;
        private List<ModeloDto> _modelos;

        [SetUp]
        public void Init()
        {
            _modeloServiceMock = new Mock<IModeloService>();
            _modelos = RetornarModelosDto();
            _modeloServiceMock.Setup(x => x.ObtenerModelos()).Returns(_modelos);
            _modeloServiceMock.Setup(x => x.ObtenerModeloPorCodigo(It.IsAny<string>())).Returns(_modelos.ElementAt(0));
            _modelosController = new ModelosController(_modeloServiceMock.Object);
        }

        [Test]
        public void DevuelvoUnaListaDeModelosCuandoLlamoAlServicio()
        {
            var result = _modelosController.ObtenerModelos();
            Assert.AreEqual(result.Count,2);
            Assert.AreEqual(result.ElementAt(0).Codigo, "I70");
            Assert.AreEqual(result.ElementAt(1).Codigo, "I40");
        }

        [Test]
        public void DevuelvoUnModeloCuandoBuscoPorCodigo()
        {
            var result = _modelosController.ObtenerModelo("I70");
            Assert.IsNotNull(result);
            Assert.AreEqual(result.Codigo, "I70");
        }

        [Test]
        public void SeDebeLlamarAlServicioUnaVezCuandoSeAgregaUnModelo()
        {
            _modelosController.AgregarModelo(RetornarModelos().ElementAt(0));
            _modeloServiceMock.Verify(x => x.AgregarModelo(It.IsAny<Modelo>()), Times.Once);
        }

        [Test]
        public void SeDebeLlamarAlServicioUnaVezCuandoSeModificaUnModelo()
        {
            _modelosController.ModificarModelo(RetornarModelos().ElementAt(0));
            _modeloServiceMock.Verify(x => x.ModificarModelo(It.IsAny<Modelo>()), Times.Once);
        }

        private List<ModeloDto> RetornarModelosDto()
        {
            return new List<ModeloDto>
            {
                new ModeloDto {Capacidad = "4GB", Marca = new Marca {Id = Guid.NewGuid(), Nombre = "IPhone"}, Codigo = "I70", Id = Guid.NewGuid(), Nombre = "S6", Color = "Rojo y Blanco", Descripcion = "Nuevo"},
                new ModeloDto {Capacidad = "8GB", Marca = new Marca {Id = Guid.NewGuid(), Nombre = "IPhone2"}, Codigo = "I40", Id = Guid.NewGuid(), Nombre = "S7", Color = "Rojo y Blanco2", Descripcion = "Nuevo2"}
            };
        }

        private List<Modelo> RetornarModelos()
        {
            return new List<Modelo>
            {
                new Modelo {Capacidad = "4GB", Marca = new Marca {Id = Guid.NewGuid(), Nombre = "IPhone"}, Codigo = "I70", Id = Guid.NewGuid(), Nombre = "S6", Color = "Rojo y Blanco", Descripcion = "Nuevo"},
                new Modelo {Capacidad = "8GB", Marca = new Marca {Id = Guid.NewGuid(), Nombre = "IPhone2"}, Codigo = "I40", Id = Guid.NewGuid(), Nombre = "S7", Color = "Rojo y Blanco2", Descripcion = "Nuevo2"}
            };
        }
    }
}
