﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using Moq;
using NUnit.Framework;
using Proyecto_Moviles.Controllers;
using Service;

namespace WebTest
{
    public class MarcasControllerTest
    {
        MarcasController _modelosController;
        private Mock<IMarcaService> _marcaServiceMock;
        private List<MarcaDto> _marcas;

        [SetUp]
        public void Init()
        {
            _marcaServiceMock = new Mock<IMarcaService>();
            _marcas = RetornarMarcasDto();
            _marcaServiceMock.Setup(x => x.ObtenerMarcas()).Returns(_marcas);
            _modelosController = new MarcasController(_marcaServiceMock.Object);
        }

        [Test]
        public void DevuelvoUnaListaDeMarcasCuandoLlamoAlServicio()
        {
            var result = _modelosController.ObtenerMarcas();
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result.ElementAt(0).Nombre, _marcas.ElementAt(0).Nombre);
            Assert.AreEqual(result.ElementAt(1).Nombre, _marcas.ElementAt(1).Nombre);
        }

        private List<MarcaDto> RetornarMarcasDto()
        {
            return new List<MarcaDto>
            {
                new MarcaDto { Id = Guid.NewGuid(), Nombre = "Samsung"},
                new MarcaDto { Id = Guid.NewGuid(), Nombre = "IPhone"}
            };
        }

        private List<Marca> RetornarModelos()
        {
            return new List<Marca>
            {
                new Marca {Id = Guid.NewGuid(), Nombre = "Samsung"},
                new Marca { Id = Guid.NewGuid(), Nombre = "IPhone"}
            };
        }
    }
}
