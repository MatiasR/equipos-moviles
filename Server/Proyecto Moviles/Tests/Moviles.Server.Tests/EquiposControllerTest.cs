﻿using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using Moq;
using NUnit.Framework;
using Proyecto_Moviles.Controllers;
using Service;

namespace WebTest
{
    public class EquiposControllerTest
    {
        EquiposController _equiposController;
        private Mock<IEquipoService> _equipoServiceMock;
        private List<EquipoDto> _marcas;

        [SetUp]
        public void Init()
        {
            _equipoServiceMock = new Mock<IEquipoService>();
            _marcas = RetornarEquiposDto();
            _equipoServiceMock.Setup(x => x.ObtenerEquipos()).Returns(_marcas);
            _equiposController = new EquiposController(_equipoServiceMock.Object);
        }

        [Test]
        public void DevuelvoUnaListaDeequiposCuandoLlamoAlServicio()
        {
            var result = _equiposController.ObtenerEquipos();
            Assert.AreEqual(result.Count, 2);
            Assert.AreEqual(result.ElementAt(0).Modelo.Nombre, _marcas.ElementAt(0).Modelo.Nombre);
            Assert.AreEqual(result.ElementAt(1).Modelo.Nombre, _marcas.ElementAt(1).Modelo.Nombre);
        }

        [Test]
        public void SeDebeLlamarAlServicioUnaVezCuandoSeModificaUnEquipo()
        {
            _equiposController.ModificarEquipo(RetornarEquipo());
            _equipoServiceMock.Verify(x => x.ModificarEquipo(It.IsAny<Equipo>()), Times.Once);
        }

        private Equipo RetornarEquipo()
        {
            return new Equipo
            {
                Id = Guid.NewGuid(),
                Descripcion = "Descripcion",
                Imei = "7-0",
                Precio = Convert.ToDecimal(7),
                Modelo = new Modelo
                {
                    Capacidad = "Capacidad",
                    Color = "Color",
                    Codigo = "Codigo",
                    Id = Guid.NewGuid(),
                    Descripcion = "Descripcion",
                    Nombre = "Nombre",
                    Marca = new Marca { Id = Guid.NewGuid(), Nombre = "Nombre" }
                }
            };
        }

        private List<EquipoDto> RetornarEquiposDto()
        {
            return new List<EquipoDto>
            {
                new EquipoDto
                {
                    Id = Guid.NewGuid(),Descripcion = "Descripcion",Imei = "7-0", Precio = Convert.ToDecimal(7),
                    Modelo = new Modelo {Capacidad = "Capacidad", Color = "Color", Codigo = "Codigo", Id = Guid.NewGuid(), Descripcion = "Descripcion", Nombre = "Nombre",
                    Marca = new Marca {Id = Guid.NewGuid(), Nombre = "Nombre"}}
                },
                new EquipoDto
                {
                    Id = Guid.NewGuid(),Descripcion = "Descripcion1",Imei = "7-00", Precio = Convert.ToDecimal(7),
                    Modelo = new Modelo {Capacidad = "Capacidad1", Color = "Color1", Codigo = "Codigo1", Id = Guid.NewGuid(), Descripcion = "Descripcion1", Nombre = "Nombre1",
                    Marca = new Marca {Id = Guid.NewGuid(), Nombre = "Nombre1"}}
                }
            };
        }
    }
}
