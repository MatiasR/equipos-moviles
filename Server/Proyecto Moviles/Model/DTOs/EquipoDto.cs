﻿using System;

namespace Model.DTOs
{
    public class EquipoDto
    {
        public  Guid Id { get; set; }
        public  string Imei { get; set; }
        public  decimal Precio { get; set; }
        public  string Descripcion { get; set; }
        public  Modelo Modelo { get; set; }
    }
}