﻿using System;
using System.Collections.Generic;

namespace Model.DTOs
{
    public class VentaDto
    {
        public Guid Id { get; set; }
        public DateTime Fecha { get; set; }
        public double Precio { get; set; }
        //public int CantidadCuotas { get; set; }
        public Cliente Cliente { get; set; }
        //public IList<Pago> Pagos { get; set; }
        public Equipo Equipo { get; set; }
    }
}