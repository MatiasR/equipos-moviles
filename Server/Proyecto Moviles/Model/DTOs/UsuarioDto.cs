﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Model.DTOs
{
    public class UsuarioDto
    {
        public Guid Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public RolEnum Rol { get; set; }
        public string Nombre { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
}