﻿using System;

namespace Model
{
    public class Pago
    {
        public virtual Guid Id { get; set; }
        public virtual double Monto { get; set; }
        public virtual DateTime Fecha { get; set; }
    }
}