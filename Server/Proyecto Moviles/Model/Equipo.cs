﻿using System;

namespace Model
{
    public class Equipo
    {
        public virtual Guid Id { get; set; }
        public virtual string Imei { get; set; }
        public virtual decimal Precio { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual Modelo Modelo { get; set; }
        public virtual Venta Venta { get; set; }
    }
}