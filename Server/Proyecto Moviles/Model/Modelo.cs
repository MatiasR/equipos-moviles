﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class Modelo
    {
        public virtual Guid Id { get; set; }
        public virtual string Codigo { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Color { get; set; }
        public virtual string Capacidad { get; set; }
        public virtual string Descripcion { get; set; }
        public virtual Marca Marca { get; set; }
        public virtual ICollection<Equipo> Equipos { get; set; } = new List<Equipo>();
    }
}