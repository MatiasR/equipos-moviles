﻿using System.Collections.Generic;
using System.Linq;
using Model;
using Model.DTOs;
using NHibernate;

namespace Datos.DAO
{
    public class ClienteDao
    {
        private ISessionFactory _sessionFactory;

        public ClienteDao(ISessionFactory sessionFactory)
        {
            _sessionFactory =   sessionFactory;
        }

        private ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }

        //Agregar
        public void Agregar(Cliente cliente)
        {
            CurrentSession.Save(cliente);
        }

        //Modificar
        public void Modificar(Cliente cliente)
        {
            CurrentSession.Update(cliente);
            CurrentSession.Flush();
        }

        //Obtener todos los clientes
        public IList<ClienteDto> Obtener()
        {
            var clientes = CurrentSession.QueryOver<Cliente>().List<Cliente>().Select(ClienteToDto).ToList();
            return clientes;
        }

        //Obtener el cliente por DNI
        public IList<ClienteDto> ObtenerPorDni(string dni)
        {
                var clientes = CurrentSession.QueryOver<Cliente>().Where(c => c.Dni == dni).List<Cliente>().Select(ClienteToDto).ToList(); ;
                return clientes;
        }

        private ClienteDto ClienteToDto(Cliente cliente)
        {
            return new ClienteDto
            {
                Nombre = cliente.Nombre,
                Apellido = cliente.Apellido,
                Date = cliente.Date,
                Direccion = cliente.Direccion,
                Dni = cliente.Dni,
                Email = cliente.Email,
                Id = cliente.Id,
                Telefono = cliente.Telefono
            };
        }
    }
}