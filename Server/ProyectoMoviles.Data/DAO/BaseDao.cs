using System.Collections.Generic;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;

namespace Datos.DAO
{
    public interface IReadOnlyDao<TEntity, TId> where TEntity : class
    {
        TEntity Obtener(TId id);

        TEntity BuscarPorId(TId id);

        IList<TEntity> BuscarTodos();

        void Flush();
    }

    public interface IDao<TEntity, TId> : IReadOnlyDao<TEntity, TId> where TEntity : class
    {
        TId Agregar(TEntity entity);

        void Actualizar(TEntity entity);

        void Borrar(TEntity entity);

        void GuardarOActualizar(TEntity entity);
    }


    public abstract class BaseDao<T, TId> : IDao<T,TId> where T : class
    {
        protected readonly ISessionFactory _sessionFactory;

        protected ISession CurrentSession
        {
            get { return _sessionFactory.GetCurrentSession(); }
        }

        public BaseDao(ISessionFactory sessionFactory)
        {
            _sessionFactory = sessionFactory;
        }

        public T Obtener(TId id)
        {
            return this.CurrentSession.Get<T>((object)id);
        }

        public T BuscarPorId(TId id)
        {
            return this.CurrentSession.QueryOver<T>().Where((ICriterion)Restrictions.IdEq((object)id)).SingleOrDefault();

        }

        public IList<T> BuscarTodos()
        {
            return this.CurrentSession.QueryOver<T>().TransformUsing(Transformers.DistinctRootEntity).List<T>();

        }

        public void Flush()
        {
           this.CurrentSession.Flush();
        }

        public TId Agregar(T entity)
        {
            return (TId)this.CurrentSession.Save((object)entity);
        }

        public void Actualizar(T entity)
        {
            this.CurrentSession.Update((object)entity);
        }

        public void Borrar(T entity)
        {
            this.CurrentSession.Delete((object)entity);
        }

        public void GuardarOActualizar(T entity)
        {
            this.CurrentSession.SaveOrUpdate((object)entity);
        }
    }
}