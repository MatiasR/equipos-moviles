﻿using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class UsuarioMapper : ClassMapping<Usuario>
    {
        public UsuarioMapper()
        {
            Id(x => x.Id, mapper => mapper.Generator(Generators.GuidComb));
            Property(x => x.UserName);
            Property(x => x.ExpireDate);
            Property(x => x.Password);
            Property(x => x.Token);
            Property(x => x.Nombre);
            Property(x => x.Apellido);
            Property(x => x.Rol);
        }
    }
}