﻿using Model;
using NHibernate.Mapping;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class ModeloMapper : ClassMapping<Modelo>
    {
        public ModeloMapper()
        {
            Id(x => x.Id, d => d.Generator(Generators.GuidComb));
            Property(x => x.Codigo);
            Property(x => x.Nombre);
            Property(x => x.Color);
            Property(x => x.Capacidad);
            Property(x => x.Descripcion);

            ManyToOne<Marca>(x => x.Marca, c => { c.Cascade(Cascade.Persist); });
            Bag<Equipo>(x => x.Equipos, c => { c.Inverse(true); }, r => { r.OneToMany(); });
        }
    }
}