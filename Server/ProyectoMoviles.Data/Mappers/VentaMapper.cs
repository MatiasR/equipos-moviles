using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class VentaMapper : ClassMapping<Venta>
    {
        public VentaMapper()
        {
            Id(x => x.Id, mapper => mapper.Generator(Generators.GuidComb));
            Property(x => x.Fecha);
            Property(x => x.CantidadCuotas);
            Property(x => x.Precio);
            //Bag<Pago>(x => x.Pagos, c => { c.Inverse(true); }, r => { r.OneToMany(); });
            ManyToOne<Cliente>(x => x.Cliente, c => { c.Cascade(Cascade.Persist); });
        }
    }
}