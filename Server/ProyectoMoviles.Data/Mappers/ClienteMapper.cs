﻿using Model;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace Datos.Mappers
{
    public class ClienteMapper : ClassMapping<Cliente>
    {
        public ClienteMapper()
        {
            Id(x => x.Id, mapper => mapper.Generator(Generators.GuidComb));
            Property(x => x.Nombre);
            Property(x => x.Apellido);
            Property(x => x.Telefono);
            Property(x => x.Dni);
            Property(x => x.Email);
            Property(x => x.Direccion);
            Property(x => x.Date);
        }
    }
}