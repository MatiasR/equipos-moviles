using System;
using System.Collections.Generic;

namespace Model
{
    public class Venta
    {
        public virtual Guid Id { get; set; }
        public virtual DateTime Fecha { get; set; }
        public virtual double Precio { get; set; }
        public virtual int CantidadCuotas { get; set; }
        public virtual Cliente Cliente { get; set; }
        public virtual ICollection<Equipo> Equipos { get; set; } = new List<Equipo>();
    }
}