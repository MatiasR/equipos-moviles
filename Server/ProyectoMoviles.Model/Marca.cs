﻿using System;
using System.Collections.Generic;

namespace Model
{
    public class Marca
    {
        public virtual Guid Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual ICollection<Modelo> Modelos { get; set; } = new List<Modelo>();
    }
}