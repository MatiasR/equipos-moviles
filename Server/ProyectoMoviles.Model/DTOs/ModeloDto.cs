﻿using System;

namespace Model.DTOs
{
    public class ModeloDto
    {
        public Guid Id { get; set; }
        public string Codigo { get; set; }
        public string Nombre { get; set; }
        public string Color { get; set; }
        public string Capacidad { get; set; }
        public string Descripcion { get; set; }
        public Marca Marca { get; set; }
    }
}