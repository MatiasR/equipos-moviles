﻿using System;

namespace Model.DTOs
{
    public class MarcaDto
    {
        public  Guid Id { get; set; }
        public  string Nombre { get; set; }
    }
}