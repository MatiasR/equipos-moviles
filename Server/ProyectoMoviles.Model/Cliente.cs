﻿using System;
using System.Collections.Generic;

namespace Model
{
    public abstract class Persona
    {
        public virtual Guid Id { get; set; }
        public virtual string Nombre { get; set; }
        public virtual string Apellido { get; set; }
        public virtual string Telefono { get; set; }
        public virtual string Dni { get; set; }
        public virtual string Email { get; set; }
        public virtual string Direccion { get; set; }
    }

    public class Cliente : Persona
    {
        public virtual string Date { get; set; }
        public virtual IList<Venta> Ventas { get; set; }
    }

    public class Administrador : Persona
    {

    }

    public class Usuario : Persona
    {
        public virtual Guid Id { get; set; }
        public virtual string UserName { get; set; }
        public virtual string Password { get; set; }
        public virtual Guid? Token { get; set; }
        public virtual DateTime ExpireDate { get; set; }
        public virtual RolEnum Rol { get; set; }
    }
}