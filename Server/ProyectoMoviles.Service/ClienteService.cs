﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datos.DAO;
using Model;
using Model.DTOs;

namespace Service
{
    public interface IClienteService
    {
        IList<ClienteDto> ObtenerClientes();
        ClienteDto ObtenerCliente(string dni);
        void Agregarcliente(Cliente cliente);
        void ModificarCliente(Cliente cliente);
    }
    public class ClienteService : IClienteService
    {
        private readonly ClienteDao _clienteDao;
        public ClienteService(ClienteDao clienteDao)
        {
            _clienteDao = clienteDao;
        }

        public IList<ClienteDto> ObtenerClientes()
        {
            return _clienteDao.Obtener();
        }

        public ClienteDto ObtenerCliente(string dni)
        {
            var clientes = _clienteDao.ObtenerPorDni(dni).FirstOrDefault();
            return clientes;
        }

        public void Agregarcliente(Cliente cliente)
        {
            _clienteDao.Agregar(cliente);
        }

        public void ModificarCliente(Cliente cliente)
        {
            _clienteDao.Modificar(cliente);
        }

    }
}