﻿using System;
using System.Collections.Generic;
using Datos.DAO;
using Model;
using Model.DTOs;

namespace Service
{
    public interface IMarcaService
    {
        IList<MarcaDto>ObtenerMarcas();
    }
    public class MarcaService : IMarcaService
    {
        private readonly IMarcaDao _marcaDao;
        public MarcaService(IMarcaDao modeloDao)
        {
            _marcaDao = modeloDao;
        }
        public IList<MarcaDto> ObtenerMarcas()
        {
            return _marcaDao.Obtener();
            //return new List<Marca> {new Marca {Nombre = "Samsung", Id = new Guid()} };

        }
    }
}